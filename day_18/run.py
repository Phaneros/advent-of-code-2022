from typing import *

from common import load_input
import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    board_size = 23
    board = np.zeros((board_size, board_size, board_size), dtype=np.int8)
    for line in lines:
        coords = [int(x) for x in line.split(',')]
        board[coords[0] + 1, coords[1] + 1, coords[2] + 1] = 1
    result = 0
    result += np.count_nonzero(np.diff(board, axis=0))
    result += np.count_nonzero(np.diff(board, axis=1))
    result += np.count_nonzero(np.diff(board, axis=2))
    print(result)
    # @ans: 3610

    # Part 2
    # flood fill from 0,0,0 to see what we can reach
    directions = [
        np.array([0, 0, +1], dtype=np.int8),
        np.array([0, 0, -1], dtype=np.int8),
        np.array([0, +1, 0], dtype=np.int8),
        np.array([0, -1, 0], dtype=np.int8),
        np.array([+1, 0, 0], dtype=np.int8),
        np.array([-1, 0, 0], dtype=np.int8),
    ]
    reachable = np.zeros(board.shape, dtype=np.int8)
    positions = [np.array([0,0,0], dtype=np.int8)]
    while len(positions):
        position = positions.pop(0)
        for direction in directions:
            new_pos = position + direction
            if np.any(new_pos < 0) or np.any(new_pos >= board_size):
                continue
            if board[new_pos[0], new_pos[1], new_pos[2]] == 0 and reachable[new_pos[0], new_pos[1], new_pos[2]] == 0:
                reachable[new_pos[0], new_pos[1], new_pos[2]] = 1
                positions.append(new_pos)

    solid = np.logical_not(reachable)

    p2_result = 0
    p2_result += np.count_nonzero(np.diff(solid, axis=0))
    p2_result += np.count_nonzero(np.diff(solid, axis=1))
    p2_result += np.count_nonzero(np.diff(solid, axis=2))
    print(p2_result)
    # @ans: 2082


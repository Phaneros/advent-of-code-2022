from typing import *

from common import load_input
import numpy as np

np.set_printoptions(linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    x_value = 1
    cycle = 0
    next_cycle_of_interest: int = 20
    values_of_interest: Dict[int, int] = {}
    

    for line in lines:
        if 'noop' in line:
            cycle += 1
            if cycle > next_cycle_of_interest:
                values_of_interest[next_cycle_of_interest] = x_value
                next_cycle_of_interest += 40
        elif line.startswith('addx'):
            instruction, value = line.split(' ')
            cycle += 2
            if cycle >= next_cycle_of_interest:
                values_of_interest[next_cycle_of_interest] = x_value
                next_cycle_of_interest += 40
            x_value += int(value)
        else:
            assert False
        # print(f'After {line}, X={x_value}, after cycle={cycle}')
    
    print(values_of_interest)
    print(sum(x * values_of_interest[x] for x in values_of_interest.keys()))
    # @ans: 13920

    # Part 2
    crt_width = 40
    crt_height = 6
    # 0-indexed
    cycle = 0
    x_value = 1

    pixels = np.zeros((crt_width, crt_height), dtype=np.int8) - 1
    for line in lines:
        if cycle >= crt_width * crt_height:
            break
        if 'noop' in line:
            this_x = cycle % crt_width
            this_y = cycle // crt_width
            if np.abs(x_value - this_x) <= 1:
                pixels[this_x, this_y] = 1
            else:
                pixels[this_x, this_y] = 0
            cycle += 1
        elif line.startswith('addx'):
            this_x = cycle % crt_width
            this_y = cycle // crt_width
            if np.abs(x_value - this_x) <= 1:
                pixels[this_x, this_y] = 1
            else:
                pixels[this_x, this_y] = 0
            cycle += 1
            this_x = cycle % crt_width
            this_y = cycle // crt_width
            if np.abs(x_value - this_x) <= 1:
                pixels[this_x, this_y] = 1
            else:
                pixels[this_x, this_y] = 0

            value = line.split(' ')[1]
            cycle += 1
            x_value += int(value)
        else:
            assert False
    for row in pixels.T:
        for item in row:
            if item: print('#', end='')
            else: print(' ', end='')
        print()
    # @ans: EGLHBLFJ




#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");

    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');
    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (LineNumber == Lines->Length - 1 && Line.Length == 0)
        {
            continue;
        }
        mmstring_println(Line);
    }

    return 0;
}

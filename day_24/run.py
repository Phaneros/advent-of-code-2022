from typing import *

from common import load_input
import numpy as np
import sys
from dataclasses import dataclass
import time

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    width = len(lines[0])
    height = len(lines)
    valley_dimensions = np.array([width-2, height-2], dtype=np.int32)

    UP = np.array([0, -1], dtype=np.int32)
    DOWN = np.array([0, +1], dtype=np.int32)
    LEFT = np.array([-1, 0], dtype=np.int32)
    RIGHT = np.array([+1, 0], dtype=np.int32)
    NO_OP = np.zeros((2,), dtype=np.int32)
    directions = (UP, DOWN, LEFT, RIGHT, NO_OP)
    direction_map = {
        'v': DOWN,
        '^': UP,
        '<': LEFT,
        '>': RIGHT,
    }

    board = np.zeros((width, height), dtype=np.int8)
    blizzard_positions = np.zeros(((width * height), 2), dtype=np.int32)
    blizzard_velocities = np.zeros(((width * height), 2), dtype=np.int32)
    num_blizzards = 0

    for y, line in enumerate(lines):
        for x, character in enumerate(line):
            if character == '.':
                pass
            elif character == '#':
                board[x, y] = 1
            elif character in '^<v>':
                blizzard_positions[num_blizzards, :] = [x, y]
                blizzard_velocities[num_blizzards, :] = direction_map[character]
                num_blizzards += 1
            else:
                assert False

    blizzard_positions = blizzard_positions[:num_blizzards, :]
    blizzard_velocities = blizzard_velocities[:num_blizzards, :]

    # A*?
    @dataclass(frozen=True)
    class State:
        position: Tuple[int, int]
        time: int
    
    initial_pos = (1, 0)
    target_pos = (width - 2, height - 1)
    parents: Dict[State, State] = {}
    
    # lower is better
    def state_min_future_cost(state: State) -> int:
        return abs(state.position[0] - target_pos[0]) + abs(state.position[1] - target_pos[0])
    
    def total_cost(state: State) -> int:
        return state.time + state_min_future_cost(state)
    
    def actual_mod(a: np.ndarray, b: np.ndarray) -> np.ndarray:
        return ((a % b) + b) % b
    
    def recover_history(state: State) -> List[State]:
        return_value = [state]
        parent_state = parents[return_value[0]]
        while parent_state is not None:
            return_value = [parent_state] + return_value
            parent_state = parents.get(return_value[0])
        return return_value

    start_time = time.perf_counter()

    state_queue = [State(initial_pos, 0)]
    states_expanded = 0
    while True:
        state = state_queue.pop(0)
        states_expanded += 1
        if state.position == target_pos:
            break
        if states_expanded % 1000 == 0:
            print(f'Expanded {states_expanded} states, current={state}, time={time.perf_counter() - start_time}s')
        next_time = state.time + 1
        current_blizzards = actual_mod(blizzard_positions + next_time * blizzard_velocities - 1, valley_dimensions) + 1
        for direction in directions:
            new_pos = state.position + direction
            if np.all(new_pos >= 0) and board[new_pos[0], new_pos[1]] == 0 and not (new_pos == current_blizzards).min(1).max():
            # if np.all(new_pos >= 0) and board[new_pos[0], new_pos[1]] == 0 and tuple(new_pos) not in current_blizzards:
                new_state = State(tuple(new_pos), next_time)
                state_queue.append(new_state)
                parents[new_state] = state
        state_queue = sorted(set(state_queue), key=total_cost)
        # print(state_queue)
    
    end_time = time.perf_counter()
    print(f'Took {end_time - start_time}s')
    parent_chain = recover_history(state)
    print('\n'.join(str(x) for x in parent_chain))
    print(state)
    # @ans: 242
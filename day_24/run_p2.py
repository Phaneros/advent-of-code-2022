from typing import *

from common import load_input
import numpy as np
import sys
from dataclasses import dataclass
import time

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    width = len(lines[0])
    height = len(lines)
    valley_dimensions = np.array([width-2, height-2], dtype=np.int32)

    UP = np.array([0, -1], dtype=np.int32)
    DOWN = np.array([0, +1], dtype=np.int32)
    LEFT = np.array([-1, 0], dtype=np.int32)
    RIGHT = np.array([+1, 0], dtype=np.int32)
    NO_OP = np.zeros((2,), dtype=np.int32)
    directions = (UP, DOWN, LEFT, RIGHT, NO_OP)
    direction_map = {
        'v': DOWN,
        '^': UP,
        '<': LEFT,
        '>': RIGHT,
    }

    board = np.zeros((width, height+1), dtype=np.int8)
    board[:, height] = 1
    # blizzard_positions = np.zeros(((width * height), 2), dtype=np.int32)
    # blizzard_velocities = np.zeros(((width * height), 2), dtype=np.int32)
    horizontal_blizzards = np.zeros(((width * height), 3), dtype=np.int32)
    vertical_blizzards = np.zeros(((width * height), 3), dtype=np.int32)
    num_vertical_blizzards = 0
    num_horizontal_blizzards = 0

    for y, line in enumerate(lines):
        for x, character in enumerate(line):
            if character == '.':
                pass
            elif character == '#':
                board[x, y] = 1
            elif character in '^v':
                vertical_blizzards[num_vertical_blizzards, :] = [x, y, (1 if character == 'v' else -1)]
                num_vertical_blizzards += 1
            elif character in '<>':
                horizontal_blizzards[num_horizontal_blizzards, :] = [x, y, (1 if character == '>' else -1)]
                num_horizontal_blizzards += 1
            else:
                assert False

    horizontal_blizzards = horizontal_blizzards[:num_horizontal_blizzards, :]
    vertical_blizzards = vertical_blizzards[:num_vertical_blizzards, :]
    vertical_blizzards = np.array(sorted(vertical_blizzards, key=tuple))

    vertical_first_index_by_x: Dict[int, int] = {}
    vertical_first_index_by_x[0] = 0
    vertical_first_index_by_x[1] = 0
    vertical_first_index_by_x[width-2] = vertical_blizzards.shape[0]
    vertical_first_index_by_x[width-1] = vertical_blizzards.shape[0]
    vertical_first_index_by_x[width] = vertical_blizzards.shape[0]
    for x in range(2, width - 2):
        vertical_first_index_by_x[x] = np.argwhere(vertical_blizzards[:, 0] == x)[0].reshape(())

    horizontal_first_index_by_y: Dict[int, int] = {}
    horizontal_first_index_by_y[-1] = 0
    horizontal_first_index_by_y[0] = 0
    horizontal_first_index_by_y[height-1] = horizontal_blizzards.shape[0]
    horizontal_first_index_by_y[height] = horizontal_blizzards.shape[0]
    horizontal_first_index_by_y[height+1] = horizontal_blizzards.shape[0]
    for y in range(1, height - 1):
        horizontal_first_index_by_y[y] = np.argwhere(horizontal_blizzards[:, 1] == y)[0].reshape(())

    # A*
    @dataclass(frozen=True)
    class State:
        position: Tuple[int, int]
        time: int
        trek_num: int
    
    initial_pos = (1, 0)
    target_pos = (width - 2, height - 1)
    targets = [target_pos, initial_pos, target_pos, target_pos]
    total_distance = target_pos[0] - initial_pos[0] + target_pos[1] - initial_pos[1]
    last_trek_index = 2
    parents: Dict[State, State] = {}
    
    # lower is better
    def state_min_future_cost(state: State) -> int:
        current_target = targets[state.trek_num]
        return abs(state.position[0] - current_target[0]) + abs(state.position[1] - current_target[0]) + max(last_trek_index - state.trek_num, 0) * total_distance
    
    def total_cost(state: State) -> int:
        return state.time + state_min_future_cost(state)
    
    def actual_mod(a: np.ndarray, b: np.ndarray) -> np.ndarray:
        return ((a % b) + b) % b
    
    def recover_history(state: State) -> List[State]:
        return_value = [state]
        parent_state = parents[return_value[0]]
        while parent_state is not None:
            return_value = [parent_state] + return_value
            parent_state = parents.get(return_value[0])
        return return_value

    start_time = time.perf_counter()

    state_queue = [State(initial_pos, 0, 0)]
    states_expanded = 0
    added_states = set(state_queue)
    while True:
        state = state_queue.pop()
        states_expanded += 1
        if state.position == target_pos and state.trek_num == last_trek_index + 1:
            break
        if states_expanded % 1000 == 0:
            print(f'Expanded {states_expanded} states, current={state}, time={time.perf_counter() - start_time}s')
        next_time = state.time + 1
        close_vertical_blizzards = vertical_blizzards[vertical_first_index_by_x[state.position[0] - 1]:vertical_first_index_by_x[state.position[0] + 2], :].copy()
        close_horizontal_blizzards = horizontal_blizzards[horizontal_first_index_by_y[state.position[1] - 1]:horizontal_first_index_by_y[state.position[1] + 2], :].copy()

        close_vertical_blizzards[:, 1] += next_time * close_vertical_blizzards[:, 2]
        close_vertical_blizzards[:, 1] = actual_mod(close_vertical_blizzards[:, 1] - 1, valley_dimensions[1]) + 1
        close_vertical_blizzards = close_vertical_blizzards[:, :2]

        close_horizontal_blizzards[:, 0] += next_time * close_horizontal_blizzards[:, 2]
        close_horizontal_blizzards[:, 0] = actual_mod(close_horizontal_blizzards[:, 0] - 1, valley_dimensions[0]) + 1
        close_horizontal_blizzards = close_horizontal_blizzards[:, :2]


        for direction in directions:
            new_pos = state.position + direction
            if (np.all(new_pos >= 0)
                and board[new_pos[0], new_pos[1]] == 0
                and not (new_pos == close_horizontal_blizzards).min(1).max()
                and not (new_pos == close_vertical_blizzards).min(1).max()
            ):
            # if np.all(new_pos >= 0) and board[new_pos[0], new_pos[1]] == 0 and tuple(new_pos) not in current_blizzards:
                new_pos = tuple(new_pos)
                new_state = State(new_pos, next_time, state.trek_num + int(new_pos == targets[state.trek_num]))
                if new_state not in added_states:
                    added_states.add(new_state)
                    state_queue.append(new_state)
                    parents[new_state] = state
        state_queue = sorted(state_queue, key=total_cost, reverse=True)
        # print(state_queue)
    
    end_time = time.perf_counter()
    print(f'Took {end_time - start_time}s')
    parent_chain = recover_history(state)
    print('\n'.join(str(x) for x in parent_chain))
    print(state)
    # @ans: 720
    # That took 1800s = 30minutes lol

from typing import *

from common import load_input
import numpy as np


if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    width = len(lines[0])
    height = len(lines)
    bounds = np.array([width, height])
    heightmap = np.zeros((width, height), dtype=np.int32)
    starting_point = np.array([-1, -1])
    ending_point = np.array([-1, -1])

    for y, line in enumerate(lines):
        for x, character in enumerate(line):
            if character == 'S':
                character = 'a'
                starting_point = np.array([x, y])
            elif character == 'E':
                character = 'z'
                ending_point = np.array([x, y])
            assert (ord(character) >= ord('a'))
            assert (ord(character) <= ord('z'))
            heightmap[x, y] = ord(character) - ord('a')
    assert tuple(starting_point) != (-1, -1)
    assert tuple(ending_point) != (-1, -1)

    # from start to end
    # move left, right, up, or down
    # elevation max one up, unlimited down

    # simple A*
    directions = [
        np.array([+1, 0]),
        np.array([-1, 0]),
        np.array([0, +1]),
        np.array([0, -1]),
    ]
    visited = np.zeros(heightmap.shape, dtype=np.bool8)

    expected_distance = sum(np.abs(starting_point - ending_point))
    search_stack = [
        # max forward cost, cost so far, id, from
        (expected_distance, 0, starting_point.copy(), None),
    ]
    expanded = 0
    while True:
        expected_distance, current_cost, this_point, from_point = search_stack.pop(0)
        if visited[this_point[0], this_point[1]]:
            continue
        expanded += 1
        this_height = heightmap[this_point[0], this_point[1]]
        visited[this_point[0], this_point[1]] = True
        if tuple(this_point) == tuple(ending_point):
            break
        for direction in directions:
            check_point = this_point + direction
            if (np.any(check_point < 0) or np.any(check_point >= bounds)):
                continue
            check_height = heightmap[check_point[0], check_point[1]]
            if (check_height <= this_height + 1) and not (visited[check_point[0], check_point[1]]):
                expected_distance = sum(np.abs(check_point - ending_point))
                search_stack.append((expected_distance, current_cost+1, check_point, this_point))
        search_stack = sorted(search_stack, key=lambda x: x[1] + x[0])
    print(current_cost)
    print(f'Expanded {expanded} nodes')
    # @ans: 394

    # Part 2
    # BFS from ending point to any 'a'?
    search_stack = [
        # cost so far, current point, from point
        (0, ending_point, None),
    ]
    visited[:] = False
    while True:
        current_cost, this_point, from_point = search_stack.pop(0)
        if visited[this_point[0], this_point[1]]:
            continue
        this_height = heightmap[this_point[0], this_point[1]]
        visited[this_point[0], this_point[1]] = True
        if this_height == 0:
            # success condition
            break
        for direction in directions:
            check_point = this_point + direction
            if (np.any(check_point < 0) or np.any(check_point >= bounds)):
                continue
            check_height = heightmap[check_point[0], check_point[1]]
            if (check_height >= this_height - 1) and not (visited[check_point[0], check_point[1]]):
                search_stack.append((current_cost+1, check_point, this_point))
    print(current_cost)
    # @ans: 388



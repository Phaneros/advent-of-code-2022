from typing import *

from common import load_input
import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    initial_numbers = tuple(int(x) for x in lines)

    def reconstruct(initial_numbers: Iterable[int], indices: List[int]) -> List[int]:
        result = [0 for x in range(len(initial_numbers))]
        for x in range(len(initial_numbers)):
            result[indices[x]] = initial_numbers[x]
        return result

    def mix_numbers(initial_numbers: Iterable[int]) -> List[int]:
        input_size = len(initial_numbers)
        indices = np.array([x for x in range(input_size)], dtype=np.int64)

        for original_index in range(input_size):
            current_index = indices[original_index]
            original_value = initial_numbers[original_index]
            if original_value == 0:
                continue
            new_index = (current_index + original_value)
            new_index %= (input_size - 1)
            if new_index <= 0:
                new_index += input_size - 1
            indices[indices > current_index] -= 1
            indices[indices >= new_index] += 1
            assert not np.any(indices < 0)
            assert not np.any(indices >= input_size)
            indices[original_index] = new_index
        return reconstruct(initial_numbers, indices)
    
    def mix_indices(initial_numbers: Iterable[int], indices: np.ndarray) -> List[int]:
        input_size = len(initial_numbers)

        for original_index in range(input_size):
            current_index = indices[original_index]
            original_value = initial_numbers[original_index]
            if original_value == 0:
                continue
            new_index = (current_index + original_value)
            new_index %= (input_size - 1)
            if new_index <= 0:
                new_index += input_size - 1
            # while new_index <= 0:
            #     new_index = new_index + input_size - 1
            # while new_index >= input_size:
            #     new_index = new_index - input_size + 1
            indices[indices > current_index] -= 1
            indices[indices >= new_index] += 1
            indices[original_index] = new_index
            assert new_index < input_size
            assert new_index >= 0
            assert not np.any(indices < 0)
            assert not np.any(indices >= input_size)
        return indices

    mixed = mix_numbers(initial_numbers)
    indices_of_0 = [index for index, value in enumerate(mixed) if value == 0]
    assert len(indices_of_0) == 1
    index_of_0 = indices_of_0[0]

    # print(mixed)
    result = 0
    for ind in (1000, 2000, 3000):
        value_at = mixed[(ind + index_of_0) % len(mixed)]
        result += value_at
        print(f'{ind}: {value_at}')
    print(result)
    # @ans: 3473

    # Part 2
    print('=== part 2 ===')
    decryption_key = 811589153

    actual_initial = tuple(decryption_key * x for x in initial_numbers)
    indices = np.array([x for x in range(len(actual_initial))], dtype=np.int64)
    for x in range(10):
        indices = mix_indices(actual_initial, indices)
    mixed = reconstruct(actual_initial, indices)
    # print(mixed)
    result = 0
    for ind in (1000, 2000, 3000):
        value_at = mixed[(ind + index_of_0) % len(mixed)]
        result += value_at
        print(f'{ind}: {value_at}')
    print(result)
    # @ans: 7496649006261

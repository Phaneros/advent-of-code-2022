from typing import *

from common import load_input
import numpy as np
import sys
import re
from dataclasses import dataclass
import time

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

@dataclass
class Blueprint:
    id: int
    ore_robot_cost: np.ndarray
    clay_robot_cost: np.ndarray
    obsidian_robot_cost: np.ndarray
    geode_robot_cost: np.ndarray

if __name__ == '__main__':
    start_time = time.perf_counter()
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')

    line_pattern = re.compile(r'Blueprint (\d+): Each ore robot costs (\d+) ore\. Each clay robot costs (\d+) ore\. Each obsidian robot costs (\d+) ore and (\d+) clay\. Each geode robot costs (\d+) ore and (\d+) obsidian\.')

    blueprints: List[Blueprint] = []
    for line in lines:
        match = re.match(line_pattern, line)
        assert match is not None
        blueprints.append(Blueprint(
            id=int(match.group(1)),
            ore_robot_cost=np.array([int(match.group(2)), 0, 0, 0], dtype=np.int32),
            clay_robot_cost=np.array([int(match.group(3)), 0, 0, 0], dtype=np.int32),
            obsidian_robot_cost=np.array([int(match.group(4)), int(match.group(5)), 0, 0], dtype=np.int32),
            geode_robot_cost=np.array([int(match.group(6)), 0, int(match.group(7)), 0], dtype=np.int32),
        ))
        # print(blueprints[-1])
    

    time_minutes = 24

    NO_RESOURCE = np.array([0, 0, 0, 0], dtype=np.int32)
    ORE = np.array([1, 0, 0, 0], dtype=np.int32)
    CLAY = np.array([0, 1, 0, 0], dtype=np.int32)
    OBSIDIAN = np.array([0, 0, 1, 0], dtype=np.int32)
    GEODE = np.array([0, 0, 0, 1], dtype=np.int32)
    resource_deltas = [
        ORE, CLAY, OBSIDIAN, GEODE
    ]

    @dataclass(frozen=True)
    class State:
        resources: Tuple[int, int, int, int]
        robots: Tuple[int, int, int, int]
        time: int
        next_robot: int
    
    state_values: Dict[State, int] = {}
    state_parents: Dict[State, State] = {}
    state_max_values: Dict[State, int] = {}


    def get_history(state: State) -> List[State]:
        return_value = [state]
        parent = state_parents.get(return_value[0])
        while parent:
            return_value = [parent] + return_value
            parent = state_parents.get(return_value[0])
        return return_value
    
    def try_get_immediate_value(state: State) -> int:
        if state_max_values.get(state) is not None:
            return state_max_values[state]
        if state.time == time_minutes:
            max_value = state.resources[3]
            state_max_values[state] = max_value
            return max_value
        if state.time + 1 == time_minutes:
            max_value = state.resources[3] + state.robots[3]
            state_max_values[state] = max_value
            return max_value
        return -1
    
    def fast_child_states(state: State, blueprint: Blueprint) -> List[State]:
        robots = np.array(state.robots, dtype=np.int32)
        resources = np.array(state.resources, dtype=np.int32)
        new_robots = (
            (ORE if resources[0] >= blueprint.ore_robot_cost[0] and state.next_robot == 0 else NO_RESOURCE)
            + (CLAY if resources[0] >= blueprint.clay_robot_cost[0] and state.next_robot == 1 else NO_RESOURCE)
            + (OBSIDIAN if resources[1] >= blueprint.obsidian_robot_cost[1] else NO_RESOURCE)
            + (GEODE if resources[2] >= blueprint.geode_robot_cost[2] else NO_RESOURCE)
        )
        robot_costs = np.array((
            blueprint.ore_robot_cost[0] * new_robots[0] + blueprint.clay_robot_cost[0] * new_robots[1],
            blueprint.obsidian_robot_cost[1] * new_robots[2],
            blueprint.geode_robot_cost[2] * new_robots[3],
            0),
            dtype=np.int32
        )
        if np.any(new_robots):
            return [
                State(
                    tuple(resources + robots - robot_costs),
                    tuple(robots + new_robots),
                    state.time + 1,
                    0
                ),
                State(
                    tuple(resources + robots - robot_costs),
                    tuple(robots + new_robots),
                    state.time + 1,
                    1
                ),
            ]
        else:
            return [State(
                tuple(resources + robots),
                state.robots,
                state.time + 1,
                state.next_robot
            )]

    def calculate_max_value(state: State, blueprint: Blueprint) -> int:
        return_value = try_get_immediate_value(state)
        if return_value >= 0:
            return return_value
        queued_states = [state]
        while queued_states:
            parent_state = queued_states[-1]
            parent_value = try_get_immediate_value(parent_state)
            if parent_value >= 0:
                queued_states.pop()
            child_states = fast_child_states(parent_state, blueprint)
            child_max_value = -1
            child_min_value = 1000
            for child_state in child_states:
                child_value = try_get_immediate_value(child_state)
                if child_value < 0:
                    queued_states.append(child_state)
                child_max_value = max(child_max_value, child_value)
                child_min_value = min(child_min_value, child_value)
            if child_min_value >= 0:
                state_max_values[parent_state] = child_max_value
                queued_states.pop()
        return state_max_values[state]

    
    best_value_for_blueprint: Dict[int, int] = {}
    for blueprint in blueprints:
        states = [
            State((0, 0, 0, 0), (1, 0, 0, 0), 0, 0),
            State((0, 0, 0, 0), (1, 0, 0, 0), 0, 1),
        ]
        best_value = 0
        best_state = None
        best_chain = []

        state_values = {}
        state_parents = {}
        state_max_values = {}
        robot_costs = [
            blueprint.ore_robot_cost,
            blueprint.clay_robot_cost,
            blueprint.obsidian_robot_cost,
            blueprint.geode_robot_cost
        ]
        while (len(states)):
            state = states.pop()
            this_state_max_value = calculate_max_value(state, blueprint)
            if this_state_max_value <= best_value:
                continue
            print(f'B={blueprint.id} | {len(states)} | {state}')
            robots = np.array(state.robots, dtype=np.int32)
            resources = np.array(state.resources, dtype=np.int32)
            new_resources = resources + robots
            if state.time >= time_minutes:
                state_values[state] = resources[3]
                if state.resources[3] > best_value:
                    best_value = state.resources[3]
                    best_state = state
                    best_chain = get_history(best_state)
                    best_values_at_step = [state_max_values[x] for x in best_chain]
                continue
            if np.all(resources >= robot_costs[state.next_robot]):
                next_states = (
                    State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 0),
                    State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 1),
                    State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 2),
                    State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 3),
                )
                for next_state in next_states:
                    state_parents[next_state] = state
                states.extend(next_states)
            else:
                next_state = State(tuple(new_resources), state.robots, state.time + 1, state.next_robot)
                state_parents[next_state] = state
                states.append(next_state)
        print('\n'.join(f'{index}: {state_max_values[x]} {x}' for index, x in enumerate(best_chain)))
        print(f'Blueprint {blueprint.id} has best value {best_value}')
        best_value_for_blueprint[blueprint.id] = best_value

        check_time = time.perf_counter()
        print(f'Took {check_time - start_time} s so far')

    sum_of_qualities = sum(k * v for k, v in best_value_for_blueprint.items())
    print(best_value_for_blueprint)
    print(sum_of_qualities)
    # @ans: 1703

    end_time = time.perf_counter()
    print(f'Took {check_time - start_time} s')

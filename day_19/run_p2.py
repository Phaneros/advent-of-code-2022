from typing import *

from common import load_input
import numpy as np
import sys
import re
from dataclasses import dataclass
import time

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

@dataclass
class Blueprint:
    id: int
    ore_robot_cost: np.ndarray
    clay_robot_cost: np.ndarray
    obsidian_robot_cost: np.ndarray
    geode_robot_cost: np.ndarray

if __name__ == '__main__':
    start_time = time.perf_counter()
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')

    line_pattern = re.compile(r'Blueprint (\d+): Each ore robot costs (\d+) ore\. Each clay robot costs (\d+) ore\. Each obsidian robot costs (\d+) ore and (\d+) clay\. Each geode robot costs (\d+) ore and (\d+) obsidian\.')

    blueprints: List[Blueprint] = []
    for line in lines[:3]:
        match = re.match(line_pattern, line)
        assert match is not None
        blueprints.append(Blueprint(
            id=int(match.group(1)),
            ore_robot_cost=np.array([int(match.group(2)), 0, 0, 0], dtype=np.int32),
            clay_robot_cost=np.array([int(match.group(3)), 0, 0, 0], dtype=np.int32),
            obsidian_robot_cost=np.array([int(match.group(4)), int(match.group(5)), 0, 0], dtype=np.int32),
            geode_robot_cost=np.array([int(match.group(6)), 0, int(match.group(7)), 0], dtype=np.int32),
        ))
        # print(blueprints[-1])
    

    time_minutes = 32

    NO_RESOURCE = np.array([0, 0, 0, 0], dtype=np.int32)
    ORE = np.array([1, 0, 0, 0], dtype=np.int32)
    CLAY = np.array([0, 1, 0, 0], dtype=np.int32)
    OBSIDIAN = np.array([0, 0, 1, 0], dtype=np.int32)
    GEODE = np.array([0, 0, 0, 1], dtype=np.int32)
    resource_deltas = [
        ORE, CLAY, OBSIDIAN, GEODE
    ]

    @dataclass(frozen=True)
    class State:
        resources: Tuple[int, int, int, int]
        robots: Tuple[int, int, int, int]
        time: int
        next_robot: int
    
    state_values: Dict[State, int] = {}
    state_parents: Dict[State, State] = {}
    state_max_values: Dict[State, int] = {}
    abs_max_values: Dict[State, int] = {}


    def get_history(state: State) -> List[State]:
        return_value = [state]
        parent = state_parents.get(return_value[0])
        while parent:
            return_value = [parent] + return_value
            parent = state_parents.get(return_value[0])
        return return_value
    
    def abs_max_value(state: State, blueprint: Blueprint) -> int:
        start_state = State(state.resources, state.robots, state.time, 0)
        try_get = abs_max_values.get(start_state)
        if try_get is not None:
            return try_get
        current_state = start_state
        while current_state.time + 3 < time_minutes:
            robots = np.array(current_state.robots, dtype=np.int32)
            resources = np.array(current_state.resources, dtype=np.int32)
            can_buy_obsidian = resources[1] >= blueprint.obsidian_robot_cost[1]
            can_buy_geode = resources[2] >= blueprint.obsidian_robot_cost[2]
            current_state = State(
                tuple(
                    resources
                    + robots
                    - (CLAY * blueprint.obsidian_robot_cost[1] if can_buy_obsidian else NO_RESOURCE)
                    - (OBSIDIAN * blueprint.geode_robot_cost[2] if can_buy_geode else NO_RESOURCE)
                ),
                tuple(robots
                + ORE
                + CLAY
                + (OBSIDIAN if can_buy_obsidian else NO_RESOURCE)
                + (GEODE if can_buy_geode else NO_RESOURCE)
                ),
                current_state.time + 1,
                0
            )
        return_value = try_get_immediate_max_value(current_state, blueprint)
        abs_max_values[start_state] = return_value
        return return_value

    def try_get_immediate_max_value(state: State, blueprint: Blueprint) -> int:
        if state_max_values.get(state) is not None:
            return state_max_values[state]
        elif state.time == time_minutes:
            max_value = state.resources[3]
        elif state.time + 1 == time_minutes:
            max_value = state.resources[3] + state.robots[3]
        elif state.time + 2 == time_minutes:
            max_value = (
                state.resources[3] + state.robots[3] * 2
                + (1 if np.all(state.resources >= blueprint.geode_robot_cost) else 0)
            )
        elif state.time + 3 == time_minutes:
            robots = np.array(state.robots, dtype=np.int32)
            resources = np.array(state.resources, dtype=np.int32)
            can_afford_next = np.all(resources >= blueprint.geode_robot_cost)
            can_afford_in_two = np.all(
                (resources + state.robots) >= ((1 + can_afford_next) * blueprint.geode_robot_cost)
            )
            max_value = (
                state.resources[3]
                + state.robots[3] * 3
                + (2 if can_afford_next else 0)
                + (1 if can_afford_in_two else 0)
            )
        # elif state.time + 4 == time_minutes:
        #     robots = np.array(state.robots, dtype=np.int32)
        #     resources = np.array(state.resources, dtype=np.int32)
        #     can_afford_next = np.all(resources >= blueprint.geode_robot_cost)
        #     can_afford_in_two = np.all(
        #         (resources + state.robots) >= ((1 + can_afford_next) * blueprint.geode_robot_cost)
        #     )
        #     cant_afford = np.all(
        #         (resources + 2 * robots + 1) < blueprint.geode_robot_cost
        #     ) and not can_afford_in_two
        #     max_value = (
        #         state.resources[3]
        #         + state.robots[3] * 4
        #         + (3 if can_afford_next else 0)
        #         + (2 if can_afford_in_two else 0)
        #         + (0 if cant_afford else 1)
        #     )
        else:
            return -1
        state_max_values[state] = max_value
        return max_value
    
    def fast_child_states(state: State, blueprint: Blueprint) -> List[State]:
        robots = np.array(state.robots, dtype=np.int32)
        resources = np.array(state.resources, dtype=np.int32)
        new_robots = (
            (ORE if resources[0] >= blueprint.ore_robot_cost[0] and state.next_robot == 0 else NO_RESOURCE)
            + (CLAY if resources[0] >= blueprint.clay_robot_cost[0] and state.next_robot == 1 else NO_RESOURCE)
            + (OBSIDIAN if resources[1] >= blueprint.obsidian_robot_cost[1] else NO_RESOURCE)
            + (GEODE if resources[2] >= blueprint.geode_robot_cost[2] else NO_RESOURCE)
        )
        robot_costs = np.array((
            blueprint.ore_robot_cost[0] * new_robots[0] + blueprint.clay_robot_cost[0] * new_robots[1],
            blueprint.obsidian_robot_cost[1] * new_robots[2],
            blueprint.geode_robot_cost[2] * new_robots[3],
            0),
            dtype=np.int32
        )
        if np.any(new_robots):
            return [
                State(
                    tuple(resources + robots - robot_costs),
                    tuple(robots + new_robots),
                    state.time + 1,
                    0
                ),
                State(
                    tuple(resources + robots - robot_costs),
                    tuple(robots + new_robots),
                    state.time + 1,
                    1
                ),
            ]
        else:
            return [State(
                tuple(resources + robots),
                state.robots,
                state.time + 1,
                state.next_robot
            )]
        
    def calculate_max_value(state: State, blueprint: Blueprint) -> int:
        return_value = try_get_immediate_max_value(state, blueprint)
        if return_value >= 0:
            return return_value
        queued_states = [state]
        while queued_states:
            parent_state = queued_states[-1]
            parent_value = try_get_immediate_max_value(parent_state, blueprint)
            if parent_value >= 0:
                queued_states.pop()
                continue
            child_states = fast_child_states(parent_state, blueprint)
            child_max_value = -1
            child_min_value = 1000
            for child_state in child_states:
                child_value = try_get_immediate_max_value(child_state, blueprint)
                if child_value < 0:
                    queued_states.append(child_state)
                child_max_value = max(child_max_value, child_value)
                child_min_value = min(child_min_value, child_value)
            if child_min_value >= 0:
                state_max_values[parent_state] = child_max_value
                queued_states.pop()
        return state_max_values[state]

    
    best_value_for_blueprint: Dict[int, int] = {}
    start_states = (
        State((0, 0, 0, 0), (1, 0, 0, 0), 0, 0),
        State((0, 0, 0, 0), (1, 0, 0, 0), 0, 1),
    )
    for blueprint in blueprints:
        best_value = -1
        best_state = None
        best_chain = []

        state_values = {}
        state_parents = {}
        state_max_values = {}
        abs_max_values = {}
        robot_costs = [
            blueprint.ore_robot_cost,
            blueprint.clay_robot_cost,
            blueprint.obsidian_robot_cost,
            blueprint.geode_robot_cost
        ]

        target_value = max(calculate_max_value(x, blueprint) for x in start_states)
        while best_state is None and target_value > 0:
            states = list(start_states)
            best_value = target_value
            print(f'Targeting {target_value}')
            while (len(states)):
                state = states.pop()
                if abs_max_value(state, blueprint) < best_value:
                    continue
                this_state_max_value = calculate_max_value(state, blueprint)
                assert state in start_states or this_state_max_value <= calculate_max_value(state_parents[state], blueprint)
                if this_state_max_value < best_value:
                    continue
                print(f'B={blueprint.id} | {len(states)} | max_val={this_state_max_value} | Best={best_value} | Target={target_value} | {state}')
                robots = np.array(state.robots, dtype=np.int32)
                resources = np.array(state.resources, dtype=np.int32)
                new_resources = resources + robots
                if state.time >= time_minutes:
                    state_values[state] = resources[3]
                    if state.resources[3] > best_value:
                        best_value = state.resources[3]
                        best_state = state
                        best_chain = get_history(best_state)
                        best_values_at_step = [state_max_values[x] for x in best_chain]
                    continue
                if np.all(resources >= robot_costs[state.next_robot]):
                    next_states = (
                        State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 3),
                        State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 2),
                        State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 1),
                        State(tuple(new_resources - robot_costs[state.next_robot]), tuple(robots+resource_deltas[state.next_robot]), state.time + 1, 0),
                    )
                    for next_state in next_states:
                        state_parents[next_state] = state
                    states.extend(sorted(next_states, key=lambda x: abs_max_value(x, blueprint)))
                else:
                    next_state = State(tuple(new_resources), state.robots, state.time + 1, state.next_robot)
                    state_parents[next_state] = state
                    states.append(next_state)
            if best_state is None:
                target_value -= 1
        print('\n'.join(f'{index}: {state_max_values[x]} {x}' for index, x in enumerate(best_chain)))
        print(f'Blueprint {blueprint.id} has best value {best_value}')
        best_value_for_blueprint[blueprint.id] = best_value

        check_time = time.perf_counter()
        print(f'Took {check_time - start_time} s so far')
        print(f'explored {len(state_max_values)} states')

    print(best_value_for_blueprint)
    assert len(best_value_for_blueprint) == 3
    prod = best_value_for_blueprint[1] * best_value_for_blueprint[2] * best_value_for_blueprint[3]
    print(prod)
    # @ans: 5301
            
    end_time = time.perf_counter()
    print(f'Took {check_time - start_time} s')

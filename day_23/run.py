from typing import *

from common import load_input
import numpy as np
import sys

from dataclasses import dataclass

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

@dataclass
class Elf:
    position: np.ndarray


if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    # lines = load_input.load_local_input(__file__, 'test2.txt')
    
    initial_width = len(lines[0])
    initial_height = len(lines)
    board = np.zeros((initial_width, initial_height), dtype=np.int8)

    elves = np.zeros((initial_width * initial_height, 2), dtype=np.int32)
    num_elves = 0

    NORTH = 0
    SOUTH = 1
    WEST = 2
    EAST = 3
    direction_order: Tuple[int, int, int, int] = (NORTH, SOUTH, WEST, EAST)

    check_positions = {
        NORTH: (np.array([0, -1], dtype=np.int32), np.array([-1, -1], dtype=np.int32), np.array([+1, -1], dtype=np.int32)),
        SOUTH: (np.array([0, +1], dtype=np.int32), np.array([-1, +1], dtype=np.int32), np.array([+1, +1], dtype=np.int32)),
        EAST:  (np.array([+1, 0], dtype=np.int32), np.array([+1, -1], dtype=np.int32), np.array([+1, +1], dtype=np.int32)),
        WEST:  (np.array([-1, 0], dtype=np.int32), np.array([-1, -1], dtype=np.int32), np.array([-1, +1], dtype=np.int32)),
    }

    for row_num, line in enumerate(lines):
        for col_num, char in enumerate(line):
            if char == '#':
                elves[num_elves, :] = np.array([col_num, row_num], dtype=np.int32)
                num_elves += 1
    
    elves = elves[:num_elves, :]
    initial_elves = elves.copy()
    print(num_elves)
    print(elves)
    display = np.array(['.', '#'])
    def print_board(board: np.ndarray):
        for row in board:
            print(''.join(display[row[x]] for x in range(len(row))))
        print('=' * board.shape[1])

    for step in range(10):
        max_x, max_y = np.max(elves, 0)
        min_x, min_y = np.min(elves, 0)
        add_board_size = 2

        if min_x == 0 or min_y == 0:
            add_board_size += 1
            elves += 1

        intentions = np.zeros((max_x + add_board_size, max_y + add_board_size), dtype=np.int32)
        intentions_by_elf = np.zeros(elves.shape, dtype=np.int32) - 1
        current_board = np.zeros(intentions.shape, dtype=np.int8)
        for elf in elves:
            current_board[elf[0], elf[1]] = 1
        # print_board(current_board.T)
        # signal intentions
        for elf_id, elf in enumerate(elves):
            # if an elf has no neighbours, no-op
            if np.sum(current_board[elf[0]-1:elf[0]+2, elf[1]-1:elf[1]+2]) == 1:
                continue
            for direction in direction_order:
                direction_clear = True
                for rel_check_position in check_positions[direction]:
                    abs_check_position = elf + rel_check_position
                    if current_board[abs_check_position[0], abs_check_position[1]] == 1:
                        direction_clear = False
                        break
                if direction_clear:
                    move_location = elf + check_positions[direction][0]
                    intentions[move_location[0], move_location[1]] += elf_id
                    intentions_by_elf[elf_id, :] = move_location
                    break
        for elf_id, intention in enumerate(intentions_by_elf):
            if not np.any(intention < 0) and intentions[intention[0], intention[1]] == elf_id:
                elves[elf_id, :] = intention
        direction_order = direction_order[1:] + (direction_order[0],)
    
    max_x, max_y = np.max(elves, 0)
    min_x, min_y = np.min(elves, 0)
    rectangle_x = max_x - min_x + 1
    rectangle_y = max_y - min_y + 1
    num_empty_spaces = rectangle_x * rectangle_y - num_elves
    print(num_empty_spaces)
    # @ans: 3862

    # Part 2
    elves = initial_elves.copy()
    direction_order = (NORTH, SOUTH, WEST, EAST)
    for step in range(1, 1000):
        # print(f'step: {step}')
        num_moved = 0
        max_x, max_y = np.max(elves, 0)
        min_x, min_y = np.min(elves, 0)
        add_board_size = 2

        if min_x == 0 or min_y == 0:
            add_board_size += 1
            elves += 1

        intentions = np.zeros((max_x + add_board_size, max_y + add_board_size), dtype=np.int32)
        intentions_by_elf = np.zeros(elves.shape, dtype=np.int32) - 1
        current_board = np.zeros(intentions.shape, dtype=np.int8)
        for elf in elves:
            current_board[elf[0], elf[1]] = 1
        # print_board(current_board.T)
        # signal intentions
        for elf_id, elf in enumerate(elves):
            # if an elf has no neighbours, no-op
            if np.sum(current_board[elf[0]-1:elf[0]+2, elf[1]-1:elf[1]+2]) == 1:
                continue
            for direction in direction_order:
                direction_clear = True
                for rel_check_position in check_positions[direction]:
                    abs_check_position = elf + rel_check_position
                    if current_board[abs_check_position[0], abs_check_position[1]] == 1:
                        direction_clear = False
                        break
                if direction_clear:
                    move_location = elf + check_positions[direction][0]
                    intentions[move_location[0], move_location[1]] += elf_id
                    intentions_by_elf[elf_id, :] = move_location
                    break
        for elf_id, intention in enumerate(intentions_by_elf):
            if not np.any(intention < 0) and intentions[intention[0], intention[1]] == elf_id:
                elves[elf_id, :] = intention
                num_moved += 1
        direction_order = direction_order[1:] + (direction_order[0],)
        if num_moved == 0:
            break
    print('== Part 2 ==')
    print(step)
    # @ans: 913


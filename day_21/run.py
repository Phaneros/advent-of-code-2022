from typing import *

from common import load_input
import numpy as np
import sys
from dataclasses import dataclass
import re

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

@dataclass
class Expr:
    id: str
    expr: str
    is_literal: bool
    operation: Optional[str]
    dependencies: Tuple[str, str] | Tuple[None]

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    expressions: Dict[str, Expr] = {}

    line_pattern = re.compile(r'(\w\w\w\w): (.*)')
    for line in lines:
        match = re.match(line_pattern, line)
        assert match is not None
        id, expr = match.groups()
        is_literal = expr.isnumeric()
        if is_literal:
            dependencies = ()
            operation = None
        else:
            dependencies = (expr[:4], expr[-4:])
            operation = expr[5]
            assert operation in ('-', '+', '/', '*')
        expressions[id] = Expr(id, expr, is_literal, operation, dependencies)
    # print(expressions)

    operations = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x // y,
    }

    values: Dict[str, int] = {}
    job_stack = ['root']
    while job_stack:
        id = job_stack[-1]
        expression = expressions[id]
        if expression.is_literal:
            values[id] = int(expression.expr)
            job_stack.pop()
        else:
            dependent_values = [values.get(x) for x in expression.dependencies]
            pending = [x is None for x in dependent_values]
            if not any(pending):
                values[id] = operations[expression.operation](*dependent_values)
                job_stack.pop()
            else:
                for dependency, is_pending in zip(expression.dependencies, pending, strict=True):
                    if is_pending:
                        job_stack.append(dependency)
    print(values['root'])
    # @ans: 10037517593724

    # Part 2
    # find what depends on humn
    my_id = 'humn'
    root_expression = expressions['root']

    depends_on_humn: Dict[str, bool] = {}
    job_stack = ['root']
    while job_stack:
        id = job_stack[-1]
        expression = expressions[id]
        if expression.id == my_id:
            depends_on_humn[id] = True
            job_stack.pop()
        elif expression.is_literal:
            depends_on_humn[id] = False
            job_stack.pop()
        else:
            dependent_values = [depends_on_humn.get(x) for x in expression.dependencies]
            pending = [x is None for x in dependent_values]
            if not any(pending):
                depends_on_humn[id] = dependent_values[0] or dependent_values[1]
                job_stack.pop()
            else:
                for dependency, is_pending in zip(expression.dependencies, pending, strict=True):
                    if is_pending:
                        job_stack.append(dependency)
    # print(depends_on_humn)
    # print('\n'.join(f'{item}: {depends_on_humn[item[0]]}' for item in expressions.items()))

    uncontrolled_dependency, = [x for x in root_expression.dependencies if not depends_on_humn[x]]
    controlled_dependency, = [x for x in root_expression.dependencies if depends_on_humn[x]]
    assert controlled_dependency is not None
    assert uncontrolled_dependency is not None

    target_value = values[uncontrolled_dependency]
    target_values = {
        controlled_dependency: target_value
    }

    inverse_operation = {
        # op, humn is left operand
        ('+', True): lambda target, other: target - other,
        ('+', False): lambda target, other: target - other,
        ('-', True): lambda target, other: target + other,
        ('-', False): lambda target, other: other - target,
        ('*', True): lambda target, other: target // other,
        ('*', False): lambda target, other: target // other,
        ('/', True): lambda target, other: target *  other,  # technically range
        ('/', False): lambda target, other: other // target, # technically range
    }

    current_id = controlled_dependency
    while current_id != my_id:
        print(f'{current_id}: {depends_on_humn[current_id]}')
        expression = expressions[current_id]
        dependency_map = tuple([depends_on_humn[x] for x in expression.dependencies])
        assert any(dependency_map)
        assert any(not x for x in dependency_map)
        depends_left = dependency_map[0]

        if depends_left:
            dependent_id, independent_id = expression.dependencies
        else:
            independent_id, dependent_id = expression.dependencies

        operation = expression.operation
        target_value = inverse_operation[operation, depends_left](target_value, values[independent_id])

        current_id = dependent_id
        
    print(target_value)
    # @ans: 3272260914328



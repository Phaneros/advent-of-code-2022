from typing import *

from common import load_input
import numpy as np

class Operation:
    def __init__(self, expression: str) -> None:
        parts = expression.split(' ')
        assert len(parts) == 5
        assert parts[0] == 'new'
        assert parts[1] == '='
        assert parts[2] == 'old'
        self.type = parts[3]
        self.right = parts[4]
    def apply(self, old: int) -> int:
        if self.right == 'old':
            right = old
        else:
            right = int(self.right)
        if self.type == '*':
            return old * right
        elif self.type == '+':
            return old + right
        else:
            assert False
    def __str__(self) -> str:
        return f'Op({self.type} {self.right})'
    def __repr__(self) -> str:
        return str(self)

class Monkey:
    def __init__(self, id: int) -> None:
        self.id = id
        self.starting_items: List[int] = []
        self.current_items: List[int] = []
        self.operation: Operation = ''
        self.test_dividor: int = -1
        self.true_target: int = -1
        self.false_target: int = -1
        self.inspected_items: int = 0
    def __str__(self) -> str:
        return (
            f'Monkey(id={self.id},'
            # f' starting_items={self.starting_items},'
            f' current_items={self.current_items},'
            f' operation={self.operation},'
            f' test_dividor={self.test_dividor},'
            f' true_target={self.true_target},'
            f' false_target={self.false_target}'
            f' inspected_items={self.inspected_items}'
            ')'
        )
    def __repr__(self) -> str:
        return str(self)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    parsing_monkey = Monkey(0)
    monkeys = [parsing_monkey]

    monkey_str = 'Monkey '
    items_str = 'Starting items: '
    operation_str = 'Operation: '
    test_str = 'Test: divisible by '
    case_true_str = 'If true: throw to monkey '
    case_false_str = 'If false: throw to monkey '

    for line in lines:
        if not line:
            parsing_monkey = Monkey(parsing_monkey.id + 1)
            monkeys.append(parsing_monkey)
        elif line.startswith(monkey_str):
            assert parsing_monkey.id == int(line[len(monkey_str):-1])
        elif line.startswith(items_str):
            parsing_monkey.starting_items = [int(x) for x in line[len(items_str):].split(', ')]
            parsing_monkey.current_items = [int(x) for x in line[len(items_str):].split(', ')]
        elif line.startswith(operation_str):
            parsing_monkey.operation = Operation(line[len(operation_str):])
        elif line.startswith(test_str):
            parsing_monkey.test_dividor = int(line[len(test_str):])
        elif line.startswith(case_true_str):
            parsing_monkey.true_target = int(line[len(case_true_str):])
        elif line.startswith(case_false_str):
            parsing_monkey.false_target = int(line[len(case_false_str):])
        else:
            assert False, f'Invalid line: {line}'
    # print('\n'.join(str(x) for x in monkeys))

    for round in range(20):
    # for round in range(1):
        for monkey in monkeys:
            for item in monkey.current_items:
                worry_level = monkey.operation.apply(item)
                worry_level = worry_level // 3
                if worry_level % monkey.test_dividor:
                    # not divisible
                    monkeys[monkey.false_target].current_items.append(worry_level)
                else:
                    # divisible
                    monkeys[monkey.true_target].current_items.append(worry_level)
            monkey.inspected_items += len(monkey.current_items)
            monkey.current_items = []
        # print('\n'.join(str(x) for x in monkeys))
        # print()
    stats = sorted(x.inspected_items for x in monkeys)
    print(stats)
    print(stats[-1] * stats[-2])
    # @ans: 50172

    # Part 2
    # no more divide by 3
    ultimate_divisor = 1
    for monkey in monkeys:
        # reset
        monkey.current_items = list(monkey.starting_items)
        monkey.inspected_items = 0
        # calculate mod
        ultimate_divisor *= monkey.test_dividor

    for round in range(10000):
    # for round in range(1):
        for monkey in monkeys:
            for item in monkey.current_items:
                worry_level = monkey.operation.apply(item)
                # worry_level = worry_level // 3
                if worry_level % monkey.test_dividor:
                    # not divisible
                    monkeys[monkey.false_target].current_items.append(worry_level % ultimate_divisor)
                else:
                    # divisible
                    monkeys[monkey.true_target].current_items.append(worry_level % ultimate_divisor)
            monkey.inspected_items += len(monkey.current_items)
            monkey.current_items = []
    stats = sorted(x.inspected_items for x in monkeys)
    print(stats)
    print(stats[-1] * stats[-2])
    # @ans: 11614682178
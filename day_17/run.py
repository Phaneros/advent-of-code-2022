from typing import *

from common import load_input
import numpy as np
from dataclasses import dataclass

@dataclass
class Piece:
    bounding_box: np.ndarray
    points: List[np.ndarray]


if __name__ == '__main__':
    import sys
    np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    pieces = [
        Piece(
            np.array([4, 1], dtype=np.int8),
            [
                np.array([0, -0], dtype=np.int8),
                np.array([1, -0], dtype=np.int8),
                np.array([2, -0], dtype=np.int8),
                np.array([3, -0], dtype=np.int8),
            ]
        ),
        Piece(
            np.array([3, 3], dtype=np.int8),
            [
                np.array([1, -0], dtype=np.int8),
                np.array([0, -1], dtype=np.int8),
                np.array([1, -1], dtype=np.int8),
                np.array([2, -1], dtype=np.int8),
                np.array([1, -2], dtype=np.int8),
            ]
        ),
        Piece(
            np.array([3, 3], dtype=np.int8),
            [
                np.array([2, -0], dtype=np.int8),
                np.array([2, -1], dtype=np.int8),
                np.array([0, -2], dtype=np.int8),
                np.array([1, -2], dtype=np.int8),
                np.array([2, -2], dtype=np.int8),
            ]
        ),
        Piece(
            np.array([1, 4], dtype=np.int8),
            [
                np.array([0, -0], dtype=np.int8),
                np.array([0, -1], dtype=np.int8),
                np.array([0, -2], dtype=np.int8),
                np.array([0, -3], dtype=np.int8),
            ]
        ),
        Piece(
            np.array([2, 2], dtype=np.int8),
            [
                np.array([0, -0], dtype=np.int8),
                np.array([0, -1], dtype=np.int8),
                np.array([1, -0], dtype=np.int8),
                np.array([1, -1], dtype=np.int8),
            ]
        ),
    ]
    num_piece_types = len(pieces)

    push_delta = {
        '>': np.array([+1, 0], dtype=np.int8),
        '<': np.array([-1, 0], dtype=np.int8),
    }
    DOWN = np.array([0, -1], dtype=np.int8)

    line = lines[0]
    num_commands = len(line)

    MAX_HEIGHT = 5000
    NUM_PIECES = 2022
    WIDTH = 7
    print_symbols = np.array([' ', '#'])

    def collides(piece: Piece, arena: np.ndarray, position: np.ndarray) -> bool:
        if position[0] < 0 or position[0] + piece.bounding_box[0] > WIDTH:
            return True
        for piece_offset in piece.points:
            arena_coords = piece_offset + position
            if arena[arena_coords[0], arena_coords[1]]:
                return True
        return False

    def part_one():
        command_index = 0
        highest_point = -1
        arena = np.zeros((WIDTH, MAX_HEIGHT), dtype=np.int8)
        for piece_num in range(NUM_PIECES):
            piece = pieces[piece_num % num_piece_types]
            position = np.array([2, highest_point + 3 + piece.bounding_box[1]])
            while True:
                # try move sideways
                try_position = position + push_delta[line[command_index]]
                command_index += 1
                command_index %= num_commands
                if not collides(piece, arena, try_position):
                    position = try_position
                # try move down
                try_position = position + DOWN
                if collides(piece, arena, try_position) or (try_position[1] - piece.bounding_box[1] + 1) < 0:
                    # soldifiy piece
                    for piece_point in piece.points:
                        arena_coords = piece_point + position
                        arena[arena_coords[0], arena_coords[1]] = 1
                        if arena_coords[1] > highest_point:
                            highest_point = arena_coords[1]
                    break
                else:
                    position = try_position
            # print(print_symbols[arena[:, :10].T])
            # print()
        print(highest_point + 1)
    part_one()
    # @ans: 3179

    # Part 2
    NUM_REPETITIONS = 50
    MAX_HEIGHT = 80000 * NUM_REPETITIONS
    highest_point = -1
    arena = np.zeros((WIDTH, MAX_HEIGHT), dtype=np.int8)

    piece_num = 0
    command_index = 0
    piece = pieces[piece_num % num_piece_types]
    position = np.array([2, highest_point + 3 + piece.bounding_box[1]])
    last = highest_point + 1
    while True:
        # try move sideways
        try_position = position + push_delta[line[command_index]]
        command_index += 1
        command_index %= num_commands
        if not collides(piece, arena, try_position):
            position = try_position
        # try move down
        try_position = position + DOWN
        if collides(piece, arena, try_position) or (try_position[1] - piece.bounding_box[1] + 1) < 0:
            # soldifiy piece
            for piece_point in piece.points:
                arena_coords = piece_point + position
                arena[arena_coords[0], arena_coords[1]] = 1
                if arena_coords[1] > highest_point:
                    highest_point = arena_coords[1]
            break
        else:
            position = try_position

    # observation:
    # after 1777 pieces, the highest point is 2787 and the command pointer is 274
    # after 3512 (+1735) pieces, the highest point is 5507 (+2720) and the command pointer is 274
    # after 5247 (+1735) pieces, the highest point is 8227 (+2720) and the command pointer is 274
    # after 6982 (+1735) pieces, the highest point is 10947 (+2720) and the command pointer is 274
    # so, for any number of pieces p = 1777 + n * 1735, the height is 2787 + n * 2720
    # therefore, at 1'000'000'000'000 (1 trillion),
    #   n = (1T - 1777) // 1735
    #   R = (1T - 1777) % 1735
    #   h = 2787 + n * 2720 + (simulate for R more pieces)
    target_height = 1_000_000_000_000
    n = (target_height - 1777) // 1735
    remaining_steps = (target_height - 1777) % 1735
    accounted_for_height = 2787 + n * 2720
    simulate_to = 1777 + remaining_steps
    double_counted_height = 2787

    # (command_index, piece_type_index) -> List[(height, piece_index)]
    states = {}
    # for piece_num in range(1, num_commands * num_piece_types * NUM_REPETITIONS):
    for piece_num in range(1, simulate_to):
        piece_type_index = piece_num % num_piece_types
        piece = pieces[piece_type_index]
        position = np.array([2, highest_point + 3 + piece.bounding_box[1]])
        # states[(command_index, piece_type_index)] = states.get((command_index, piece_type_index), []) + [(highest_point+1, piece_num)]
        # if len(states[(command_index, piece_type_index)]) > 4:
        #     print(print_symbols[arena[:, highest_point - 13:highest_point+2].T])
        #     print(states[(command_index, piece_type_index)])
        #     print()
        while True:
            # try move sideways
            try_position = position + push_delta[line[command_index]]
            command_index += 1
            command_index %= num_commands
            if not collides(piece, arena, try_position):
                position = try_position
            # try move down
            try_position = position + DOWN
            assert (try_position[1] - piece.bounding_box[1] + 1) >= 0
            if collides(piece, arena, try_position):
                # soldifiy piece
                for piece_point in piece.points:
                    arena_coords = piece_point + position
                    arena[arena_coords[0], arena_coords[1]] = 1
                    if arena_coords[1] > highest_point:
                        highest_point = arena_coords[1]
                break
            else:
                position = try_position

    print(accounted_for_height + (highest_point + 1) - double_counted_height)
    # @ans: 1567723342929


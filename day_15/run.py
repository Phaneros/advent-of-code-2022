from typing import *

from common import load_input
import numpy as np
import re

def manhattan_distance(pos1: Tuple[int, int], pos2: Tuple[int, int]) -> int:
    return abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])

def overlaps(first: Tuple[int, int], second: Tuple[int, int]) -> bool:
    return not ((first[1] < second[0]) or second[1] < first[0])

def merge_append_range(ranges: List[Tuple[int, int]], new_range: Tuple[int, int]) -> List[Tuple[int, int]]:
    has_merged = True
    while has_merged:
        has_merged = False
        for index, existing_range in enumerate(ranges):
            if overlaps(existing_range, new_range):
                new_range = (min(existing_range[0], new_range[0]), max(existing_range[1], new_range[1]))
                del ranges[index]
                has_merged = True
                break
    ranges.append(new_range)
    return ranges

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    target_y = 2000000
    # lines = load_input.load_local_input(__file__, 'test.txt')
    # target_y = 10
    
    line_pattern = re.compile(r'^Sensor at x=([-\d]+), y=([-\d]+): closest beacon is at x=([-\d]+), y=([-\d]+)')
    sensors: List[Tuple[int, int]] = []
    beacons: List[Tuple[int, int]] = []
    distances: List[int] = []
    for line in lines:
        match = re.match(line_pattern, line)
        assert match is not None
        sensor_x, sensor_y, beacon_x, beacon_y = [int (x) for x in match.groups()]
        sensor_pos = (sensor_x, sensor_y)
        beacon_pos = (beacon_x, beacon_y)
        sensors.append(sensor_pos)
        beacons.append(beacon_pos)
        distances.append(manhattan_distance(sensor_pos, beacon_pos))
    
    sensors = np.array(sensors)
    beacons = np.array(beacons)
    # print(sensors)
    # print(beacons)
    
    ranges: List[Tuple[int, int]] = []
    for sensor, distance in zip(sensors, distances):
        if abs(target_y - sensor[1]) > distance:
            continue
        width = distance - abs(target_y - sensor[1])
        new_range = tuple(sorted([sensor[0] - width, sensor[0] + width]))
        merge_append_range(ranges, new_range)
    covered = 0
    for range in ranges:
        covered += range[1] - range[0]
    print(f'covered = {covered}')
    # @ans: 4827924

    # Part 2
    # max index, not size
    max_index = 4000000
    # max_index = 20
    # there should be exactly one place that accepts a beacon

    # spin the whole arena on its diagonals, to make the diamonds into squares
    # u-axis goes \
    # v-axis goes /
    arena_size = 2 * max_index + 1
    arena_bounds = (-max_index, +max_index)
    ranges_u: List[Tuple[int, int]] = []
    ranges_v: List[Tuple[int, int]] = []
    all_u_bounds: Set[int] = set()
    all_v_bounds: Set[int] = set()
    for sensor, distance in zip(sensors, distances):
        sensor_pos_u = sensor[0] - sensor[1]
        sensor_pos_v = max_index - sensor[0] - sensor[1]
        all_u_bounds.add(sensor_pos_u - distance)
        all_u_bounds.add(sensor_pos_u + distance)
        all_v_bounds.add(sensor_pos_v - distance)
        all_v_bounds.add(sensor_pos_v + distance)

        ranges_u.append((sensor_pos_u - distance, sensor_pos_u + distance))
        ranges_v.append((sensor_pos_v - distance, sensor_pos_v + distance))
    # find the point within the diamond around 0, 0 that is outside all ranges

    all_one_wide_u = set()
    all_one_wide_v = set()
    for v_bound_1 in all_v_bounds:
        for v_bound_2 in all_v_bounds:
            if abs(v_bound_1 - v_bound_2) == 2:
                all_one_wide_v.add(tuple(sorted([v_bound_1, v_bound_2])))
    for u_bound_1 in all_u_bounds:
        for u_bound_2 in all_u_bounds:
            if abs(u_bound_1 - u_bound_2) == 2:
                all_one_wide_u.add(tuple(sorted([u_bound_1, u_bound_2])))
    
    # this doesn't work for the test data, but it works for my input :D
    assert len(all_one_wide_u) == 1
    assert len(all_one_wide_v) == 1
    u_range, = all_one_wide_u
    v_range, = all_one_wide_v
    u_pos = sum(u_range) // 2
    v_pos = sum(v_range) // 2

    # don't mind the overflow costing me two guesses and a few minutes...
    x = np.int64((u_pos - v_pos + max_index) // 2)
    y = np.int64(x - u_pos)
    tuning_frequency = x * 4000000 + y
    print(x * 4 + y // 1000000, y % 1000000)
    print(tuning_frequency)
    # @ans: 12977110973564

usage = """
Usage:
    build [filename]

Builds the passed-in C file when run as a script
"""

import os
import subprocess
from typing import *

class BuildLogger:
    def __init__(self, build_log_file_location: str = 'build_log.log', print_to_stdout: bool = True) -> None:
        self.print_to_stdout = print_to_stdout
        self._build_log_file_location = build_log_file_location
        self.build_log_messages: List[str] = []
    def log(self, message: str) -> None:
        if self.print_to_stdout: print(message)
        self.build_log_messages.append(message)

    def flush_build_log(self) -> None:
        with open(self._build_log_file_location, 'w') as fp:
            for line in self.build_log_messages:
                print(line, file=fp)

def exec(cmd: List[str], cwd: str, env: Dict[str, str], logger: BuildLogger) -> int:
    logger.log(' '.join(cmd))
    proc = subprocess.run(cmd, cwd=cwd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    logger.log('=== stdout ===')
    logger.log(proc.stdout.decode('utf-8'))
    if proc.stderr:
        logger.log('=== stderr ===')
        logger.log(proc.stderr.decode('utf-8'))
    logger.log('=== end ===')
    return proc.returncode

c_env = {
    'TEMP': os.environ['TEMP'],
    'INCLUDE':
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\ATLMFC\include;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\include;'
        r'C:\Program Files (x86)\Windows Kits\10\include\10.0.17763.0\ucrt;'
        r'C:\Program Files (x86)\Windows Kits\10\include\10.0.17763.0\shared;'
        r'C:\Program Files (x86)\Windows Kits\10\include\10.0.17763.0\um;'
        r'C:\Program Files (x86)\Windows Kits\10\include\10.0.17763.0\winrt;'
        r'C:\Program Files (x86)\Windows Kits\10\include\10.0.17763.0\cppwinrt',
    'LIB':
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\ATLMFC\lib\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x64;'
        r'C:\Program Files (x86)\Windows Kits\10\lib\10.0.17763.0\ucrt\x64;'
        r'C:\Program Files (x86)\Windows Kits\10\lib\10.0.17763.0\um\x64;',
    'LIBPATH':
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\ATLMFC\lib\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\x86\store\references;'
        r'C:\Program Files (x86)\Windows Kits\10\UnionMetadata\10.0.17763.0;'
        r'C:\Program Files (x86)\Windows Kits\10\References\10.0.17763.0;'
        r'C:\Windows\Microsoft.NET\Framework64\v4.0.30319;',
    'Path':
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\bin\HostX64\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\VC\VCPackages;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Team Tools\Performance Tools\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Team Tools\Performance Tools;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\Shared\Common\VSPerfCollectionTools\\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\Shared\Common\VSPerfCollectionTools\;'
        r'C:\Program Files (x86)\Windows Kits\10\bin\10.0.17763.0\x64;'
        r'C:\Program Files (x86)\Windows Kits\10\bin\x64;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\\MSBuild\15.0\bin;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\;'
        r'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\;'
        r'C:\Windows\system32;'
        r'C:\Windows;',
    'SystemRoot': r'C:\WINDOWS'
}

def build_c(filepath: str, logger: BuildLogger) -> Tuple[bool, Optional[str]]:
    """
    @filepath: The path to the C file to build
    @return [okay, exe_filename]
    """
    if not os.path.isfile(filepath):
        logger.log(f'File does not exist: {filepath}')
        return False, None
    logger.log(f'Building {filepath}')
    filename = os.path.basename(filepath)
    filestem = os.path.splitext(filename)[0]

    dirpath = os.path.dirname(filepath)
    out_dirname = 'out'
    out_dirpath = os.path.join(dirpath, out_dirname)
    os.makedirs(out_dirpath, exist_ok=True)
    root_relpath = os.path.relpath('.', out_dirpath)
    exe_filename = 'run.exe'
    map_filename = os.path.join(out_dirpath, f'{filestem}.map')
    compiler_args = [
        'cl',
        '-nologo',
        '-Gm-',     # turn off incremental build
        '-MT',      # static link libraries instead of using dlls
        '-GR-',     # turn off runtime type information
        '-EHa-',    # No exception handling
        '-Od',      # No optimization (debug)
        '-Oi',      # output intrinsics
        '-WX',      # treat warnings as errors
        '-W4',
        '-wd4201',  # declaring nameless struct
        '-wd4100',  # unused function parameter
        '-wd4189',  # unused local variable
        '-wd4200',  # unused local variable
        '-wd4505',  # unreferenced local function
        # '-wd4996',  # sprintf usage
        '-FC',  # full pathnames in diagnostics
        '-Z7',  # enable debugging
        f'-DMM_ASSERTS',
        f'-DMM_DEBUG',
        f'-D{filestem}',
        rf'-I{root_relpath}\mmlib',
    ]
    linker_flags = [
        '-link',
        '-opt:ref',
        'user32.lib', 'gdi32.lib', 'winmm.lib'
    ]
    location_flags = [
        f'-Fe{exe_filename}',
        f'-Fm{map_filename}',
        os.path.abspath(filepath),
    ]
    args = compiler_args + location_flags + linker_flags
    retval = exec(args, out_dirpath, c_env, logger)
    return retval == 0, os.path.join(out_dirpath, exe_filename)

if __name__ == '__main__':
    import sys
    import time
    from datetime import datetime
    import atexit

    if len(sys.argv) < 2:
        print('No arguments supplied')
        print(usage)
    elif '-h' in sys.argv or '-help' in sys.argv or '--help' in sys.argv:
        print(usage)
    else:
        filepath = sys.argv[1]
        if os.path.splitext(filepath)[1] not in ('.c', '.cpp'):
            print(f'Invalid extension: {os.path.basename(filepath)}')
            sys.exit(1)
        
        start_time = time.perf_counter()
        logger = BuildLogger(os.path.join(os.path.dirname(filepath), 'out', f'build_{os.path.basename(filepath)}.log'))
        atexit.register(logger.flush_build_log)
        logger.log(datetime.now().strftime('Start time: %Y-%M-%d_%H:%m:%S'))

        okay, exe_filename = build_c(filepath, logger)
        end_time = time.perf_counter()

        logger.log(f'build took {end_time - start_time}s')

        if not okay:
            logger.log('Build failed! D:')
            sys.exit(1)
        logger.log('Build successful! :D')


from typing import *

from common import load_input
import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    for line in lines:
        print(line)

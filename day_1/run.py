from typing import *

from common import load_input
import numpy as np


if __name__ == '__main__':
    # Calorie Counting
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')

    calorie_counts_by_elf = [0]
    for line in lines:
        if not line:
            calorie_counts_by_elf.append(0)
        else:
            calorie_counts_by_elf[-1] += int(line)
    print(f'Max: {max(calorie_counts_by_elf)}')
    
    # part 2
    sorted_calorie_counts = sorted(calorie_counts_by_elf)
    print(sorted_calorie_counts[-3:])
    print(sum(sorted_calorie_counts[-3:]))




#include <stdio.h>
#include "mm_string.cpp"
#include "mm_file.cpp"
#include "mm_list.cpp"

int main()
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);
    string Input = mmfile_ReadEntireFile("input.txt");
    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    u32 MAX_CATEGORIES = 300;
    dynamic_array_u32 *GroupedCounts = (dynamic_array_u32 *)mmlist_AllocateList(&AppMemory, MAX_CATEGORIES, sizeof(s32));
    u32 GroupIndex = 0;
    
    for (s32 LineIndex = 0; LineIndex < Lines->Length; ++LineIndex)
    {
        string Line = Lines->Elements[LineIndex];
        if (mmstring_IsWhitespace(Line))
        {
            GroupIndex++;
            Assert(GroupIndex < MAX_CATEGORIES, "Array overflow\n")
        }
        else
        {
            optional_s32 EntryAmount = mmstring_ParseInt(Line);
            Assert(EntryAmount.Okay, "Invalid int format\n");
            GroupedCounts->Elements[GroupIndex] += EntryAmount.Result;
        }
    }
    printf("Maximum: %d\n", mmlist_Maximum(GroupedCounts, u32, mm_Zero));
    // @ans: should be 73211

    // Part 2
    mmlist_RadixSort(&AppMemory, GroupedCounts, 5);
    printf("Top 3: %d, %d, %d\n", GroupedCounts->Elements[MAX_CATEGORIES-1], GroupedCounts->Elements[MAX_CATEGORIES-2], GroupedCounts->Elements[MAX_CATEGORIES-3]);
    printf("Sum(Top 3): %d\n", GroupedCounts->Elements[MAX_CATEGORIES-1] + GroupedCounts->Elements[MAX_CATEGORIES-2] + GroupedCounts->Elements[MAX_CATEGORIES-3]);
    // @ans: should be 213958

    // Note(mm): Got it working at 00:45, only ~3hr 30min slower than the Python approach :X

    return 0;
}

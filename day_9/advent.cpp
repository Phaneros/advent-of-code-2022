
#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"

struct vec2
{
    union
    {
        struct
        {
            s32 X;
            s32 Y;
        };
        s32 Elements[2];
    };
};

vec2 mmvec2_ComponentwiseSign(vec2 Vector)
{
    return {
        Vector.X > 0 ? 1 : Vector.X < 0 ? -1 : 0,
        Vector.Y > 0 ? 1 : Vector.Y < 0 ? -1 : 0,
    };
}

vec2 mmvec2_Add(vec2 Vector1, vec2 Vector2)
{
    return {
        Vector1.X + Vector2.X,
        Vector1.Y + Vector2.Y,
    };
}

vec2 mmvec2_Subtract(vec2 Vector1, vec2 Vector2)
{
    return {
        Vector1.X - Vector2.X,
        Vector1.Y - Vector2.Y,
    };
}

s32 mmvec2_DistanceSquared(vec2 Vector1, vec2 Vector2)
{
    s32 XDifference = Vector1.X - Vector2.X;
    s32 YDifference = Vector1.Y - Vector2.Y;
    return XDifference * XDifference + YDifference * YDifference;
}

bool8 mmvec2_Equals(vec2 Vector1, vec2 Vector2)
{
    return (Vector1.X == Vector2.X && Vector1.Y == Vector2.Y);
}

_DEFINE_ARRAY_OF_TYPE(vec2);
void AddVisited(dynamic_array_vec2 *Visited, vec2 Position)
{
    for (s32 Index = 0; Index < Visited->Length; ++Index)
    {
        if (mmvec2_Equals(Visited->Elements[Index], Position))
        {
            return;
        }
    }
    Assert(Visited->Length < Visited->MaxLength, "Not enough space in visited set\n");
    Visited->Elements[Visited->Length] = Position;
    Visited->Length++;
}

vec2 DirectionFromId(char DirectionId)
{
    switch (DirectionId)
    {
        case 'U': return {0, +1};
        case 'D': return {0, -1};
        case 'L': return {-1, 0};
        case 'R': return {+1, 0};
        default: {
            AssertTrue(0);
            return {};
        }
    }
}

void PartOne(app_memory *AppMemory, dynamic_array_string *Lines)
{
    vec2 HeadPos = {0};
    vec2 TailPos = {0};
    dynamic_array_vec2 *Visited = (dynamic_array_vec2 *)mmlist_AllocateList(AppMemory, 10000, sizeof(vec2));
    Visited->Length = 1;

    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++ LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (Line.Length == 0)
        {
            continue;
        }
        char DirectionId = Line.Data[0];
        optional_s32 DistanceOpt = mmstring_ParseInt({Line.Length-2, &Line.Data[2]});
        AssertTrue(DistanceOpt.Okay);
        s32 Distance = DistanceOpt.Result;
        vec2 Direction = DirectionFromId(DirectionId);
        for (s32 Step = 0; Step < Distance; ++Step)
        {
            HeadPos = mmvec2_Add(HeadPos, Direction);
            vec2 TailToHead = mmvec2_Subtract(HeadPos, TailPos);
            while (mmvec2_DistanceSquared(HeadPos, TailPos) > 2)
            {
                TailPos = mmvec2_Add(TailPos, mmvec2_ComponentwiseSign(TailToHead));
                AddVisited(Visited, TailPos);
            }
        }
    }
    printf("%d\n", Visited->Length);
    // @ans: 6190
}

void PartTwo(app_memory *AppMemory, dynamic_array_string *Lines)
{
    #define NUM_SEGMENTS (10)
    #define HEAD (0)
    #define TAIL (9)
    vec2 Segments[NUM_SEGMENTS] = {};
    dynamic_array_vec2 *Visited = (dynamic_array_vec2 *)mmlist_AllocateList(AppMemory, 10000, sizeof(vec2));
    Visited->Length = 1;

    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++ LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (Line.Length == 0)
        {
            continue;
        }
        char DirectionId = Line.Data[0];
        optional_s32 DistanceOpt = mmstring_ParseInt({Line.Length-2, &Line.Data[2]});
        AssertTrue(DistanceOpt.Okay);
        s32 Distance = DistanceOpt.Result;
        vec2 Direction = DirectionFromId(DirectionId);
        for (s32 Step = 0; Step < Distance; ++Step)
        {
            Segments[HEAD] = mmvec2_Add(Segments[HEAD], Direction);
            for (s32 Segment = 1; Segment < NUM_SEGMENTS; ++Segment)
            {
                s32 AheadSegment = Segment - 1;
                vec2 TailToHead = mmvec2_Subtract(Segments[AheadSegment], Segments[Segment]);
                while (mmvec2_DistanceSquared(Segments[AheadSegment], Segments[Segment]) > 2)
                {
                    Segments[Segment] = mmvec2_Add(Segments[Segment], mmvec2_ComponentwiseSign(TailToHead));
                    if (Segment == TAIL)
                    {
                        AddVisited(Visited, Segments[Segment]);
                    }
                }
            }
        }
    }
    printf("%d\n", Visited->Length);
    // @ans: 2516
}

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");
    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    PartOne(&AppMemory, Lines);
    PartTwo(&AppMemory, Lines);

    return 0;
}

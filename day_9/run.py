from typing import *

from common import load_input
import numpy as np

def part_one(lines: List[str]) -> None:
    visited = set()
    head_pos = np.array([0, 0])
    tail_pos = np.array([0, 0])
    visited.add(tuple(tail_pos))
    for line in lines:
        direction, distance = line.split(' ')
        distance_traveled = int(distance)
        head_travels = distance_traveled * directions[direction]
        head_pos += head_travels
        
        head_to_tail = tail_pos - head_pos
        if np.linalg.norm(head_to_tail) > 1.42:
            direction_between = np.argmax(np.abs(head_to_tail))
            other_axis = (direction_between - 1) * -1
            sign_between = -np.sign(head_to_tail[direction_between])
            tail_pos[other_axis] = head_pos[other_axis]
            while np.abs(tail_pos[direction_between] - head_pos[direction_between]) > 1:
                tail_pos[direction_between] += sign_between
                visited.add(tuple(tail_pos))
    print(len(visited))

def part_two(lines: List[str]) -> None:
    visited = set()
    positions = np.zeros((10, 2))
    TAIL = 9
    HEAD = 0
    visited.add(tuple(positions[TAIL, :]))
    for line in lines:
        direction, distance = line.split(' ')
        distance_traveled = int(distance)
        head_travels = directions[direction]

        for step in range(distance_traveled):
            positions[HEAD, :] += head_travels

            for tail in range(1, TAIL + 1):
                head = tail - 1
            
                head_to_tail = positions[tail, :] - positions[head, :]
                while np.linalg.norm(head_to_tail) > 1.42:
                    positions[tail, :] -= np.sign(head_to_tail)
                    head_to_tail = positions[tail, :] - positions[head, :]
                    if tail == TAIL:
                        visited.add(tuple(positions[tail, :]))
    print(len(visited))

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    # options = 'RDLU'
    directions = {
        'R': np.array([+1, 0]),
        'L': np.array([-1, 0]),
        'U': np.array([0, +1]),
        'D': np.array([0, -1]),
    }

    part_one(lines)
    # @ans: 6190

    # Part 2
    part_two(lines)
    # @ans: 2516


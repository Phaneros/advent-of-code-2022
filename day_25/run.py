from typing import *

from common import load_input
import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    digit_values = {
        '0': 0,
        '1': 1,
        '2': 2,
        '-': -1,
        '=': -2,
    }

    sum_of_numbers = 0
    for line in lines:
        number = 0
        for character in line:
            number *= 5
            number += digit_values[character]
        sum_of_numbers += number
        # print(f'{line}: {number}')
    print(sum_of_numbers)
    
    resulting_digits = []
    place = 0
    remaining = sum_of_numbers * 5
    while remaining > 0:
        # divisor = 5 ** place
        remaining = remaining // 5
        digit = remaining % 5
        resulting_digits.append(digit)
    # currently in reversed order
    print(resulting_digits)

    for index in range(len(resulting_digits)):
        digit = resulting_digits[index]
        if digit > 2:
            resulting_digits[index] -= 5
            resulting_digits[index+1] += 1
    
    return_character_map = {
        0: '0',
        1: '1',
        2: '2',
        -1: '-',
        -2: '=',
    }
    print(''.join(return_character_map[x] for x in reversed(resulting_digits)))
    # @ans: 2=222-2---22=1=--1-2

from typing import *

from common import load_input
import numpy as np

def spaceship(left: int, right: int) -> Literal[-1, 0, 1]:
    """
    Compare function. Called spaceship because <=> looks kind of like a spaceship.
    Returns -1 if left < right, +1 if left > right, and 0 if left == right.
    """
    if left < right:
        return -1
    elif left > right:
        return 1
    else:
        return 0

def recursive_compare(left, right) -> Literal[-1, 0, 1]:
    if isinstance(left, int) and isinstance(right, int):
        return spaceship(left, right)
    elif isinstance(left, int):
        left = left,
    elif isinstance(right, int):
        right = right,
    for left_item, right_item in zip(left, right):
        return_value = recursive_compare(left_item, right_item)
        if return_value != 0:
            return return_value
    return spaceship(len(left), len(right))


def qsort(my_list: Iterable[Any], comp: Callable[[Any, Any], Literal[-1, 0, 1]]) -> List[Any]:
    if len(my_list) <= 1:
        return my_list
    if len(my_list) == 2:
        if comp(my_list[0], my_list[1]) == -1:
            return my_list
        else:
            return [my_list[1], my_list[0]]
    pivot = my_list[0]
    return (
        qsort([x for x in my_list[1:] if comp(x, pivot) <= 0], comp)
        + [pivot]
        + qsort([x for x in my_list[1:] if comp(x, pivot) > 0], comp)
    )


if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    loading_left = True
    left = ()
    right = ()
    index = 1
    sum_of_right_order = 0
    for line in lines:
        if not line:
            loading_left = True
            if recursive_compare(left, right) == -1:
                sum_of_right_order += index
            index += 1
            continue
        if loading_left:
            exec(f'left = {line}')
            loading_left = False
        else:
            exec(f'right = {line}')
    print(sum_of_right_order)
    # @ans: 6187

    # Part 2
    # sort them, find indices of [[6]], [[2]]
    all_packets = []
    for line in lines:
        if not line:
            continue
        # line = line.replace('[]', '()').replace('[', '(').replace(']', ',)')
        exec(f'all_packets.append({line})')
    all_packets.append(((2,),))
    all_packets.append(((6,),))
    all_packets = qsort(all_packets, comp=recursive_compare)
    index1, = [ind+1 for ind, x in enumerate(all_packets) if x==((2,),)]
    index2, = [ind+1 for ind, x in enumerate(all_packets) if x==((6,),)]
    print(index1 * index2)
    # @ans: 23520


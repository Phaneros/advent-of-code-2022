
#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"


#define NUM_LETTERS 26
s32 CountNonZero(s8 Histogram[NUM_LETTERS])
{
    s32 ReturnValue = 0;
    for (s32 Index = 0; Index < NUM_LETTERS; ++Index)
    {
        ReturnValue += !!Histogram[Index];
    }
    return ReturnValue;
}

void PrintFirstIndexAfterNUnique(string Input, s32 SignalLength)
{
    s8 Histogram[NUM_LETTERS] = {0};
    for (s32 CharacterIndex = 0; CharacterIndex < SignalLength; ++CharacterIndex)
    {
        Histogram[Input.Data[CharacterIndex] - 'a']++;
    }
    for (s32 CharacterIndex = SignalLength; CharacterIndex < Input.Length; ++CharacterIndex)
    {
        if (CountNonZero(Histogram) == SignalLength)
        {
            printf("Index after %d-length starter: %d\n", SignalLength, CharacterIndex);
            break;
        }
        else
        {
            Histogram[Input.Data[CharacterIndex] - 'a']++;
            Histogram[Input.Data[CharacterIndex-SignalLength] - 'a']--;
        }
    }
}

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");

    PrintFirstIndexAfterNUnique(Input, 4);
    // @ans: 1210
    PrintFirstIndexAfterNUnique(Input, 14);
    // @ans: 3476

    return 0;
}

from typing import *

from common import load_input
import numpy as np

def after_n_distinct(string: str, n: int) -> int:
    for index in range(len(line)):
        if len(set(line[index:index+n])) == n:
            print(line[index:index+n])
            print(f'start index: {index}')
            print(index + n)
            return index + n

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    line = lines[0]
    after_n_distinct(line, 4)
    # @ans: 1210
    after_n_distinct(line, 14)
    # @ans: 3476


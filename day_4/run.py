from typing import *

from common import load_input
import numpy as np

def extract_range(written_range: str) -> Tuple[int, int]:
    return [int(x) for x in written_range.split('-', 1)]

def a_contains_b(first: Tuple[int, int], second: Tuple[int, int]) -> bool:
    return (first[0] <= second[0] and first[1] >= second[1])

def overlaps(first: Tuple[int, int], second: Tuple[int, int]) -> bool:
    return not ((first[1] < second[0]) or second[1] < first[0])

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    total_contains = 0
    total_overlaps = 0
    for line in lines:
        first_range_iden, second_range_iden = line.split(',')
        first_range = extract_range(first_range_iden)
        second_range = extract_range(second_range_iden)
        if a_contains_b(first_range, second_range) or a_contains_b(second_range, first_range):
            total_contains += 1
        if overlaps(first_range, second_range):
            total_overlaps += 1
    print(total_contains)
    # @ans: 433

    # Part 2
    print(total_overlaps)
    # @ans: 852



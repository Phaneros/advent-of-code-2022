
#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"

struct range
{
    s32 start;
    s32 end;
};

bool8 Contains(range A, range B)
{
    return (A.start <= B.start && A.end >= B.end);
}

bool8 Overlaps(range A, range B)
{
    return !(A.end < B.start || B.end < A.start);
}

range ParseRange(string RangeId)
{
    s32 IndexOfSeparator = mmstring_FindFirstOccurrence(RangeId, '-');
    Assert(IndexOfSeparator >= 0, "'-' doesn't appear in range id\n");
    optional_s32 Start = mmstring_ParseInt(mmstring_Substring(RangeId, 0, IndexOfSeparator));
    Assert(Start.Okay, "Unable to parse string\n");
    optional_s32 End = mmstring_ParseInt(mmstring_Substring(RangeId, IndexOfSeparator + 1, RangeId.Length - IndexOfSeparator - 1));
    Assert(Start.Okay, "Unable to parse string\n");

    return {Start.Result, End.Result};
}

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");
    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    u32 NumberLinesWithContainedRanges = 0;
    u32 NumberLinesWithOverlappingRanges = 0;
    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (LineNumber == Lines->Length - 1 && Line.Length == 0)
        {
            continue;
        }
        dynamic_array_string *RangeIds = mmstring_Split(&AppMemory, Line, ',');
        Assert(RangeIds->Length == 2, "Expected two ranges on the line\n");
        range FirstRange = ParseRange(RangeIds->Elements[0]);
        range SecondRange = ParseRange(RangeIds->Elements[1]);

        if (Contains(FirstRange, SecondRange) || Contains(SecondRange, FirstRange))
        {
            NumberLinesWithContainedRanges++;
        }
        if (Overlaps(FirstRange, SecondRange))
        {
            NumberLinesWithOverlappingRanges++;
        }
    }
    // Part 1
    printf("Number contains: %d\n", NumberLinesWithContainedRanges);
    // @ans: 433

    // Part 2
    printf("Number overlaps: %d\n", NumberLinesWithOverlappingRanges);
    // @ans: 852

    return 0;
}

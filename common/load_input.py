
from typing import *
import os.path

def load_local_input(script_location: str, file_name: str) -> List[str]:
    dir = os.path.dirname(script_location)
    input_path = os.path.join(dir, file_name)
    with open(input_path, 'r') as fp:
        result = fp.readlines()
    return [x.strip() for x in result]

def load_local_input_nostrip(script_location: str, file_name: str) -> List[str]:
    dir = os.path.dirname(script_location)
    input_path = os.path.join(dir, file_name)
    with open(input_path, 'r') as fp:
        result = fp.readlines()
    return [x.strip('\n\r') for x in result]

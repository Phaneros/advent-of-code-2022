
#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"

#define MAX_DIRECTORIES 500
struct directory_info
{
    string Name;
    s32 Size;
};

_DEFINE_ARRAY_OF_TYPE(directory_info)

s32 IndexOfDirectory(dynamic_array_directory_info *DirectoryInfo, string DirectoryName)
{
    for (s32 Index = 0; Index < DirectoryInfo->Length; ++Index)
    {
        if (mmstring_Equals(DirectoryInfo->Elements[Index].Name, DirectoryName))
        {
            return Index;
        }
    }
    return -1;
}

void AddDirectorySize(dynamic_array_directory_info *DirectoryInfo, string DirectoryName, s32 Size)
{
    string CurrentDirectoryToAddTo = DirectoryName;
    while (CurrentDirectoryToAddTo.Length > 1)
    {
        s32 IndexToAddTo = IndexOfDirectory(DirectoryInfo, CurrentDirectoryToAddTo);
        if (IndexToAddTo < 0)
        {
            DirectoryInfo->Elements[DirectoryInfo->Length] = {CurrentDirectoryToAddTo, Size};
            DirectoryInfo->Length++;
        }
        else
        {
            DirectoryInfo->Elements[IndexToAddTo].Size += Size;
        }
        s32 IndexOfLastSlash = mmstring_FindLastOccurrence(CurrentDirectoryToAddTo, '/');
        CurrentDirectoryToAddTo.Length = IndexOfLastSlash == 0 ? 1 : IndexOfLastSlash;
    }
    Assert(CurrentDirectoryToAddTo.Length == 1, "Expected '/'\n");
    s32 IndexToAddTo = IndexOfDirectory(DirectoryInfo, CurrentDirectoryToAddTo);
    if (IndexToAddTo < 0)
    {
        DirectoryInfo->Elements[DirectoryInfo->Length] = {CurrentDirectoryToAddTo, Size};
        DirectoryInfo->Length++;
    }
    else
    {
        DirectoryInfo->Elements[IndexToAddTo].Size += Size;
    }
    Assert(DirectoryInfo->Length <= DirectoryInfo->MaxLength, "Added too many directories\n");
}

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");
    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    dynamic_array_directory_info *Directories = (dynamic_array_directory_info *)mmlist_AllocateList(&AppMemory, MAX_DIRECTORIES, sizeof(directory_info));
    Directories->Elements[0] = {STATIC_STRING("/"), 0};
    Directories->Length = 1;
    string CurrentDirectory = {};
    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (LineNumber == Lines->Length - 1 && Line.Length == 0)
        {
            continue;
        }
        // mmstring_println(Line);
        dynamic_array_string *Tokens = mmstring_Split(&AppMemory, Line, ' ');
        if (Tokens->Elements[0].Data[0] == '$')
        {
            if (mmstring_Equals(mmstring_StripWhitespace(Tokens->Elements[1]), STATIC_STRING("ls")))
            {
                continue;
            }
            else
            {
                Assert(mmstring_Equals(mmstring_StripWhitespace(Tokens->Elements[1]), STATIC_STRING("cd")), "Unknown command\n");
                if (Tokens->Elements[2].Data[0] == '/')
                {
                    CurrentDirectory = STATIC_STRING("/");
                }
                else if (mmstring_Equals(mmstring_StripWhitespace(Tokens->Elements[2]), STATIC_STRING("..")))
                {
                    CurrentDirectory.Length = mmstring_FindLastOccurrence(CurrentDirectory, '/');
                }
                else
                {
                    if (CurrentDirectory.Length == 1)
                    {
                        dynamic_array_string JoinList = {2, 2, sizeof(string), {STATIC_STRING("/"), mmstring_StripWhitespace(Tokens->Elements[2])}};
                        CurrentDirectory = mmstring_Join(
                            &AppMemory,
                            &JoinList);
                    }
                    else
                    {
                        dynamic_array_string JoinList = {3, 3, sizeof(string), {CurrentDirectory, STATIC_STRING("/"), mmstring_StripWhitespace(Tokens->Elements[2])}};
                        CurrentDirectory = mmstring_Join(
                            &AppMemory,
                            &JoinList);
                    }
                }
            }
        }
        else if (mmstring_Equals(mmstring_StripWhitespace(Tokens->Elements[0]), STATIC_STRING("dir")))
        {
            continue;
        }
        else
        {
            optional_s32 Size = mmstring_ParseInt(Tokens->Elements[0]);
            Assert(Size.Okay, "Invalid size format\n");
            AddDirectorySize(Directories, CurrentDirectory, Size.Result);
        }
    }

    printf("number of directories: %d\n", Directories->Length);
    // extract sum of sizes less than 100000
    s32 SumOfSizes = 0;
    // FILE *DebugFile;
    // s32 ErrorCode = fopen_s(&DebugFile, "cpp_debug.txt", "w");
    // AssertTrue(ErrorCode == 0);
    for (s32 Index = 0; Index < Directories->Length; ++Index)
    {
        // fprintf(DebugFile, "%.*s: ", Directories->Elements[Index].Name.Length, Directories->Elements[Index].Name.Data);
        // fprintf(DebugFile, "%d\n", Directories->Elements[Index].Size);
        if (Directories->Elements[Index].Size <= 100000)
        {
            SumOfSizes += Directories->Elements[Index].Size;
        }
    }
    printf("Sum of sizes <= 100000: %d\n", SumOfSizes);
    // @ans: 1444896

    // Part 2: Smallest directory to delete that puts total size below 40000000
    s32 SpaceNeeded = 40000000;
    s32 SpaceToDelete = Directories->Elements[0].Size - SpaceNeeded;
    s32 SmallestSizeToDelete = Directories->Elements[0].Size;
    for (s32 Index = 0; Index < Directories->Length; ++Index)
    {
        s32 ThisDirectorySize = Directories->Elements[Index].Size;
        if (ThisDirectorySize >= SpaceToDelete && ThisDirectorySize < SmallestSizeToDelete)
        {
            SmallestSizeToDelete = ThisDirectorySize;
        }
    }
    printf("Smallest size to delete: %d\n", SmallestSizeToDelete);
    // @ans: 404395

    return 0;
}

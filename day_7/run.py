from typing import *

from common import load_input
import numpy as np
import os


if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    current_directory = '/'
    files = set()
    directories_sizes = {}
    for line in lines[1:]:
        tokens = line.split()
        if tokens[0] == '$':
            if tokens[1] == 'cd':
                if tokens[2] == '/':
                    current_directory = '/'
                else:
                    current_directory = os.path.normpath(os.path.join(current_directory, tokens[2])).replace('\\', '/')
            elif tokens[1] == 'ls':
                continue
            else:
                assert False
        elif tokens[0] == 'dir':
            pass
        else:
            file_size = int(tokens[0])
            filename = tokens[1]
            if filename not in files:  # Note(mm): This check apparently isn't necessary, same answer without it
                files.add('/'.join([current_directory, filename]))
                add_dir = current_directory
                while add_dir != '/':
                    directories_sizes[add_dir] = directories_sizes.get(add_dir, 0) + file_size
                    add_dir = add_dir.rsplit('/', 1)[0] or '/'
                directories_sizes['/'] = directories_sizes.get('/', 0) + file_size

    total_sizes = 0
    # fp = open('debug.txt', 'w')
    for dirname in directories_sizes.keys():
        dirsize = directories_sizes[dirname]
        # print(f'{dirname}: {dirsize}', file=fp)
        if dirsize <= 100000:
            total_sizes += dirsize
    # fp.close()
    print(total_sizes)
    # @ans: 1444896

    total_fs_size = 70000000
    needed_space = 30000000
    used_space = directories_sizes['/']
    needed_delete = used_space + needed_space - total_fs_size

    size_to_delete = min([x for x in directories_sizes.values() if x >= needed_delete])
    print(size_to_delete)
    # @ans: 404395

    print('done')
            


from typing import *

from common import load_input
import numpy as np


if __name__ == '__main__':
    import sys
    np.set_printoptions(linewidth=1000, threshold=sys.maxsize)

    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    fall_directions = [
        np.array([0, 1], dtype=np.int32),   # down
        np.array([-1, 1], dtype=np.int32),  # down-left
        np.array([+1, 1], dtype=np.int32),  # down-right
    ]
    abs_sand_origin = (500, 0)
    paths: List[Tuple[np.ndarray, np.ndarray]] = []
    max_y = 0
    min_x = 500
    max_x = 500
    for line in lines:
        str_points = line.split('->')
        points = []
        for str_point in str_points:
            points.append(np.array([int(x) for x in str_point.split(',')]))
            if points[-1][0] > max_x:
                max_x = points[-1][0]
            if points[-1][0] < min_x:
                min_x = points[-1][0]
            if points[-1][1] > max_y:
                max_y = points[-1][1]
        for path in zip(points, points[1:]):
            paths.append((
                path[0],
                path[1],
            ))
    assert min_x > 0
    x_padding = 5
    width = max_x - min_x + 1
    height = max_y + 1

    def abs2arr(x_value: int) -> int:
        return x_value - min_x + x_padding

    sand_origin = np.array([abs2arr(abs_sand_origin[0]), abs_sand_origin[1]])
    board = np.zeros((width + 1 + x_padding * 2, height + 2), dtype=np.int32)
    for path in paths:
        x_values = sorted([path[0][0], path[1][0]])
        y_values = sorted([path[0][1], path[1][1]])
        board[abs2arr(x_values[0]):abs2arr(x_values[1]) + 1, y_values[0]:y_values[1] + 1] = 1

    completed = False
    sand_deposited = 0
    while True:
        sand_pos = sand_origin
        moved = True
        while moved:
            if sand_pos[0] < 0 or sand_pos[0] >= (width + x_padding) or sand_pos[1] > max_y:
                completed = True
                break
            moved = False
            for direction in fall_directions:
                try_pos = sand_pos + direction
                if board[try_pos[0], try_pos[1]] == 0:
                    sand_pos = try_pos
                    moved = True
                    break
        if completed:
            break
        sand_deposited += 1
        board[sand_pos[0], sand_pos[1]] = 2
    # print(board.T)
    print(sand_deposited)
    # @ans: 728

    # Part 2
    board[board == 2] = 0
    board[:, max_y + 2] = 1
    # print(board.T)

    sand_deposited = 0
    while True:
        sand_pos = sand_origin
        moved = True
        while moved:
            moved = False
            for direction in fall_directions:
                try_pos = sand_pos + direction
                if try_pos[0] < 0 or try_pos[0] >= board.shape[0]:
                    sand_deposited += max_y + 2 - try_pos[1]
                    break
                if board[try_pos[0], try_pos[1]] == 0:
                    sand_pos = try_pos
                    moved = True
                    break
        sand_deposited += 1
        board[sand_pos[0], sand_pos[1]] = 2
        if tuple(sand_pos) == tuple(sand_origin):
            break
    print(sand_deposited)
    # @ans: 27623



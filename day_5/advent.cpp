
#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"

int GetStackIndex(char StackName)
{
    AssertTrue(StackName >= '1' && StackName <= '9');
    return StackName - '1';
}

void SimulateOperations(app_memory *AppMemory, dynamic_array_string *Lines, bool8 ReverseCratesOnMove)
{
    const s32 NumberOfStacks = 9;
    const s32 MaxStartingHeight = 8;
    const s32 MaxStackHeight = NumberOfStacks * MaxStartingHeight;
    // char *Stacks[NumberOfStacks][MaxStartingHeight * NumberOfStacks] = {};
    char (*Stacks)[MaxStackHeight] = (char(*)[MaxStackHeight])mmem_AllocateObject(AppMemory, sizeof(char) * NumberOfStacks * MaxStackHeight);
    s32 StackHeights[NumberOfStacks] = {};


    s32 LineNumber = 0;
    for (; LineNumber < MaxStartingHeight; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (Line.Data[1] != ' ' && Line.Data[0] == ' ')
        {
            // got to stack identifiers, I've just hardcoded this
            Assert(LineNumber == MaxStartingHeight, "Pre-baked max starting height is wrong\n");
            break;
        }
        for (s32 StackIndex = 0; StackIndex < NumberOfStacks; ++StackIndex)
        {
            s32 SegmentStart = StackIndex * 4;
            if (Line.Data[SegmentStart] == ' ')
            {
                continue;
            }
            Stacks[StackIndex][MaxStartingHeight - LineNumber - 1] = Line.Data[SegmentStart + 1];
            StackHeights[StackIndex]++;
        }
    }
    LineNumber += 2;
    AssertTrue(LineNumber == 10);

    // for (s32 StackIndex = 0; StackIndex < NumberOfStacks; ++StackIndex)
    // {
    //     printf("stack %d: %.*s\n", StackIndex, StackHeights[StackIndex], Stacks[StackIndex]);
    // }
    for (; LineNumber < Lines->Length; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (LineNumber == Lines->Length - 1 && Line.Length == 0)
        {
            break;
        }

        // format: move xx? from y to z
        //         012345  678901234567
        const s32 AmountOffset = 5;
        const s32 PossibleSecondDigitOffset = 6;
        const s32 BaseFromOffset = 12;
        const s32 BaseToOffset = 17;
        const u8 LengthExtraAmountCharacters = mmstring_IsCharDecimal(Line.Data[PossibleSecondDigitOffset]);

        optional_s32 MaybeAmount = mmstring_ParseInt({1+LengthExtraAmountCharacters, &Line.Data[AmountOffset]});
        optional_s32 MaybeFromIndex = mmstring_ParseInt({1, &Line.Data[BaseFromOffset+LengthExtraAmountCharacters]});
        optional_s32 MaybeToIndex = mmstring_ParseInt({1, &Line.Data[BaseToOffset+LengthExtraAmountCharacters]});
        AssertTrue(MaybeAmount.Okay);
        AssertTrue(MaybeFromIndex.Okay);
        AssertTrue(MaybeToIndex.Okay);
        s32 Amount = MaybeAmount.Result;
        s32 FromIndex = MaybeFromIndex.Result - 1;
        s32 ToIndex = MaybeToIndex.Result - 1;

        // printf("%d, %d, %d\n", Amount, FromIndex, ToIndex);
        // top of the stack is at index [StackHeight-1]
        s32 MoveFromIndex = ReverseCratesOnMove ? StackHeights[FromIndex] - 1 : StackHeights[FromIndex] - Amount;
        s32 DeltaToNextIndexToMove = ReverseCratesOnMove ? -1 : 1;
        for (s32 NumberOfCratesMoved = 0; NumberOfCratesMoved < Amount; ++NumberOfCratesMoved)
        {
            Stacks[ToIndex][StackHeights[ToIndex]] = Stacks[FromIndex][MoveFromIndex];
            StackHeights[ToIndex]++;
            AssertTrue(StackHeights[ToIndex] <= MaxStackHeight);
            StackHeights[FromIndex]--;
            AssertTrue(StackHeights[ToIndex] >= 0);
            MoveFromIndex += DeltaToNextIndexToMove;
        }

        // printf("%d x %d -> %d\n", Amount, FromIndex, ToIndex);
        // for (s32 StackIndex = 0; StackIndex < NumberOfStacks; ++StackIndex)
        // {
        //     printf("stack %d (%d): %.*s\n", StackIndex, StackHeights[StackIndex], StackHeights[StackIndex], Stacks[StackIndex]);
        // }
        // printf("=====\n");
    }

    for (s32 StackIndex = 0; StackIndex < NumberOfStacks; ++StackIndex)
    {
        printf("%c", Stacks[StackIndex][StackHeights[StackIndex]-1]);
    }
    printf("\n");
}

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");

    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    SimulateOperations(&AppMemory, Lines, 1);
    // @ans: MQSHJMWNH
    SimulateOperations(&AppMemory, Lines, 0);
    // @ans: LLWJRBHVZ

    // This took like two hours because declaring the types of arrays in C is hard...

    return 0;
}

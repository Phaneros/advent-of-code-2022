from typing import *

from common import load_input
import numpy as np
import re


if __name__ == '__main__':
    lines = load_input.load_local_input_nostrip(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    # load initial state
    stacks = [[] for x in range(9)]
    id_to_index: Dict[str, int] = {}
    for line_number, line in enumerate(lines):
        if not line:
            break
        segments = [line[start:start+4] for start in range(0, len(line), 4)]
        for stack_id, segment in enumerate(segments):
            if segment[0] == '[':
                stacks[stack_id] += [segment[1]]
            elif segment[1] != ' ':
                assert len(id_to_index) == stack_id
                id_to_index[segment[1]] = stack_id
    
    stacks: List[List[str]] = [list(reversed(x)) for x in stacks]
    # print(stacks)
    # print(ids)

    command_pattern = re.compile(r'^move (\d+) from (\d+) to (\d+)$')
    for line in lines[line_number:]:
        if not line: continue
        command_parts = re.match(command_pattern, line)
        assert command_parts is not None
        amount_str, from_stack, to_stack = command_parts.groups()

        amount = int(amount_str)
        from_index = id_to_index[from_stack]
        to_index = id_to_index[to_stack]

        stacks[to_index].extend(reversed(stacks[from_index][-amount:]))
        # part 2:
        # stacks[to_index].extend((stacks[from_index][-amount:]))
        stacks[from_index] = stacks[from_index][:-amount]

    print(''.join(x[-1] for x in stacks))
    # part 1
    # @ans: MQSHJMWNH

    # part 2
    # load initial state
    stacks = [[] for x in range(9)]
    id_to_index: Dict[str, int] = {}
    for line_number, line in enumerate(lines):
        if not line:
            break
        segments = [line[start:start+4] for start in range(0, len(line), 4)]
        for stack_id, segment in enumerate(segments):
            if segment[0] == '[':
                stacks[stack_id] += [segment[1]]
            elif segment[1] != ' ':
                assert len(id_to_index) == stack_id
                id_to_index[segment[1]] = stack_id
    
    stacks: List[List[str]] = [list(reversed(x)) for x in stacks]
    # print(stacks)
    # print(ids)

    command_pattern = re.compile(r'^move (\d+) from (\d+) to (\d+)$')
    for line in lines[line_number:]:
        if not line: continue
        command_parts = re.match(command_pattern, line)
        assert command_parts is not None
        amount_str, from_stack, to_stack = command_parts.groups()

        amount = int(amount_str)
        from_index = id_to_index[from_stack]
        to_index = id_to_index[to_stack]

        stacks[to_index].extend((stacks[from_index][-amount:]))
        stacks[from_index] = stacks[from_index][:-amount]
    print(''.join(x[-1] for x in stacks))
    # @ans: LLWJRBHVZ


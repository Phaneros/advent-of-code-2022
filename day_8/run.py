from typing import *

from common import load_input
import numpy as np
from scipy import signal

def part_one(grid: np.ndarray):
    # for each direction
    # visible if tree > progressive max
    max_py = np.zeros(grid.shape)
    for rownum in range(grid.shape[0]):
        row = grid[rownum, :]
        max_py[rownum, :] = np.maximum(max_py[rownum-1, :], row)  # negative index works because we look at the last row, which is all zeros

    max_ny = np.zeros(grid.shape)
    for rownum in range(-1, -grid.shape[0]-1, -1):
        row = grid[rownum, :]
        max_ny[rownum, :] = np.maximum(max_ny[rownum+1, :], row)

    max_px = np.zeros(grid.shape)
    for colnum in range(grid.shape[1]):
        col = grid[:, colnum]
        max_px[:, colnum] = np.maximum(max_px[:, colnum-1], col)  # negative index works because we look at the last row, which is all zeros

    max_nx = np.zeros(grid.shape)
    for colnum in range(-1, -grid.shape[1]-1, -1):
        col = grid[:, colnum]
        max_nx[:, colnum] = np.maximum(max_nx[:, colnum+1], col)
    with open('data.txt', 'w') as fp:
        print(max_ny, file=fp)
    
    px_kernel = np.array([[1, -1]])
    nx_kernel = np.array([[-1, 1]])
    py_kernel = np.array([[1], [-1]])
    ny_kernel = np.array([[-1], [1]])
    assert px_kernel.shape == (1, 2)
    assert nx_kernel.shape == (1, 2)
    assert py_kernel.shape == (2, 1)
    assert ny_kernel.shape == (2, 1)
    diff_px = signal.convolve2d(max_px, px_kernel)[:, :max_px.shape[1]]
    diff_nx = signal.convolve2d(max_nx, nx_kernel)[:, 1:]
    diff_py = signal.convolve2d(max_py, py_kernel)[:max_px.shape[0], :]
    diff_ny = signal.convolve2d(max_ny, ny_kernel)[1:, :]
    
    final_mask = np.zeros(grid.shape, dtype=np.int8)
    final_mask[0, :] = 1
    final_mask[:, 0] = 1
    final_mask[-1, :] = 1
    final_mask[:, -1] = 1

    final_mask = np.logical_or(final_mask, diff_px)
    final_mask = np.logical_or(final_mask, diff_py)
    final_mask = np.logical_or(final_mask, diff_nx)
    final_mask = np.logical_or(final_mask, diff_ny)
    print(np.count_nonzero(final_mask))


if __name__ == '__main__':
    import sys
    np.set_printoptions(linewidth=1000, threshold=sys.maxsize)
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    width = len(lines[0])
    height = len(lines)
    grid = np.ndarray((width, height), dtype=np.int8)
    for row, line in enumerate(lines):
        grid[row, :] = np.array([int(x) for x in line])
    
    part_one(grid)
    # @ans: 1816

    view_nx = np.zeros(grid.shape)
    for colnum in range(grid.shape[1]):
        col = grid[:, colnum]
        can_see = col + 1
        for behind_colnum in range(colnum-1, -1, -1):
            behind_col = grid[:, behind_colnum]
            view_nx[:, colnum] += can_see > 0
            can_see = (can_see > behind_col + 1) * can_see

    view_px = np.zeros(grid.shape)
    for colnum in range(grid.shape[1]):
        col = grid[:, colnum]
        can_see = col + 1
        for behind_colnum in range(colnum+1, width):
            behind_col = grid[:, behind_colnum]
            view_px[:, colnum] += can_see > 0
            can_see = (can_see > behind_col + 1) * can_see

    view_ny = np.zeros(grid.shape)
    for rownum in range(grid.shape[0]):
        col = grid[rownum, :]
        can_see = col + 1
        for behind_rownum in range(rownum-1, -1, -1):
            behind_col = grid[behind_rownum, :]
            view_ny[rownum, :] += can_see > 0
            can_see = (can_see > behind_col + 1) * can_see

    view_py = np.zeros(grid.shape)
    for rownum in range(grid.shape[0]):
        col = grid[rownum, :]
        can_see = col + 1
        for behind_rownum in range(rownum+1, width):
            behind_col = grid[behind_rownum, :]
            view_py[rownum, :] += can_see > 0
            can_see = (can_see > behind_col + 1) * can_see

    view_scores = view_px * view_nx * view_py * view_ny

    # with open('day_8/debug.txt', 'w') as fp:
    #     print(view_scores[:10, :10], file=fp)

    print(np.max(view_scores[:]))
    # @ans: 383520


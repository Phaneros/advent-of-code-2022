
#ifndef _MM_WIN_C
#define _MM_WIN_C

#include <windows.h>
#include "mm_types.h"
#include "mm_assert.h"
#include "mm_util.cpp"

internal void mmwin_FreeMemory(void *Memory)
{
    if (Memory)
    {
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
}

/**
 * Reads the contents of an entire file.
 * Returns a string struct of size 0 if the file couldn't be opened.
 */
internal string mmwin_ReadEntireFile(const char *FileName)
{
    HANDLE FileHandle = CreateFileA(
        FileName,
        GENERIC_READ,
        FILE_SHARE_READ,
        0,
        OPEN_EXISTING,
        0, 0
    );

    string Result = {0};
    if (FileHandle == INVALID_HANDLE_VALUE)
    {
        return Result;
    }
    LARGE_INTEGER WinFileSize;
    if (!GetFileSizeEx(FileHandle, &WinFileSize))
    {
        CloseHandle(FileHandle);
        return Result;
    }

    u32 FileSize = mmutil_SafeTruncateUInt64(WinFileSize.QuadPart);
    Result.Memory = VirtualAlloc(0, FileSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    if (!Result.Memory)
    {
        CloseHandle(FileHandle);
        return Result;
    }

    DWORD BytesRead;
    if (!ReadFile(FileHandle, Result.Data, FileSize, &BytesRead, 0)) {
        CloseHandle(FileHandle);
        mmwin_FreeMemory(Result.Memory);
        Result.Memory = 0;
        return Result;
    }
    CloseHandle(FileHandle);
    Result.Length = BytesRead;
    return Result;
}

HANDLE mmwin_stdin, mmwin_stdout;

internal void mmwin_InitializeConsole()
{
    mmwin_stdin = GetStdHandle(STD_INPUT_HANDLE);
    mmwin_stdout = GetStdHandle(STD_OUTPUT_HANDLE);
    // TODO(mm): check for failure
}

internal u32 mmwin_print(string Str)
{
    DWORD BytesWritten;
    WriteConsoleA(mmwin_stdout, Str.Data, Str.Length, &BytesWritten, 0);
    return BytesWritten;
}

#ifdef mm_win
int main(int ArgCount, char **ArgList)
{
    mmwin_InitializeConsole();
    string FileContents = mmwin_ReadEntireFile("mm_win.cpp");
    Assert(FileContents.Length > 100, "File read smaller than expected\n");
    mmwin_print(STATIC_STRING("Hello, World!\n"));
    mmwin_print(FileContents);
    mmwin_FreeMemory(FileContents.Memory);

    // Test reading from a non-existent file results in null string
    FileContents = mmwin_ReadEntireFile("does_not_exist");
    Assert(FileContents.Length == 0, "Got non-zero file read from non-existed file\n");
}
#endif

#endif /* _MM_WIN_C */

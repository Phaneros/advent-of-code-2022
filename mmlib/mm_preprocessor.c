/*
Note(mm): Abandoned for now, may experiment more later.
Kind of finnicky and the need to #include header files to maintain any kind of state is kind of bad.
Better to look into metaprograms.

Utilities for using the C preprocessor
 */

#ifndef _MM_PREPROCESSOR_C
#define _MM_PREPROCESSOR_C

#include "mm_types.h"

#ifdef mm_preprocessor
#define NUM_TESTS 0
#define TESTS ()
#endif

// Note(mm): Largely based off blogposts here: http://saadahmad.ca/cc-preprocessor-metaprogramming-2/

#define MMPP_STRINGIFY(...) #__VA_ARGS__

// Note(mm): get around macro expansion inhibition
// see https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)
#define CAT3(a, b, ...) PRIMITIVE_CAT(PRIMITIVE_CAT(a, b), __VA_ARGS__)

#define MMPP_EVAL(...) _MMPP_EVAL_2(_MMPP_EVAL_2(__VA_ARGS__))
#define _MMPP_EVAL_2(...) _MMPP_EVAL_1(_MMPP_EVAL_1(__VA_ARGS__))
#define _MMPP_EVAL_1(...) __VA_ARGS__
#define MMPP_EMPTY()
#define MMPP_DEFER(...) __VA_ARGS__ MMPP_EMPTY()
#define MMPP_DEFER2(...) __VA_ARGS__ MMPP_DEFER(MMPP_EMPTY()) ()
#define MMPP_DEFER3(...) __VA_ARGS__ MMPP_DEFER2(MMPP_EMPTY()) ()

#define MMPP_DEFERRED_STRINGIFY(...) MMPP_EVAL(MMPP_DEFER(MMPP_STRINGIFY)(__VA_ARGS__))

#define MMPP_EAT(...)

// Basic logic
#define _IF_0(true, ...) __VA_ARGS__
#define _IF_1(true, ...) true
#define IF(bool_value) CAT(_IF_, bool_value)

// Optionals
// Note(mm): options are SOME(x) and NONE
#define _MMPP_EXPANDED_SOME(...) _MMPP_PLACEHOLDER, SOME(__VA_ARGS__)
// if x is SOME(xxx), returns (EXPANDED, SOME(xxx))
// else returns (MMPP_EXPANDED_xxx, NONE)
#define _MMPP_GET_OPTIONAL_AS_TUPLE(...) ( MMPP_EVAL(CAT(_MMPP_EXPANDED_, __VA_ARGS__)), NONE )

#define MMPP_SELECT_SECOND(first, second, ...) second
#define _MMPP_EXTRACT_OPTIONAL(x) MMPP_SELECT_SECOND x
#define MMPP_AS_OPTIONAL(x) MMPP_EVAL(MMPP_DEFER(MMPP_SELECT_SECOND) _MMPP_GET_OPTIONAL_AS_TUPLE(x))

#define _MMPP_IS_PRESENT_SOME(...) 1
#define _MMPP_IS_PRESENT_NONE 0
#define MMPP_IS_PRESENT(x) CAT(_MMPP_IS_PRESENT_, x)

#define _OPTIONAL_UNWRAP_SOME(...) __VA_ARGS__
#define MMPP_OPTIONAL_UNWRAP(value) CAT(_OPTIONAL_UNWRAP_, value)
#define MMPP_OPTIONAL_UNWRAP_OR_DEFAULT(value, ...) \
    IF(MMPP_EVAL(MMPP_IS_PRESENT(MMPP_AS_OPTIONAL(value)))) ( /*then*/ MMPP_OPTIONAL_UNWRAP(value), /*else*/ __VA_ARGS__ )

#include <stdio.h>
char* SOME(int a) {
    return "SOME";
}
#define TESTCASE(str, ...) printf(MMPP_STRINGIFY(__VA_ARGS__) str, __VA_ARGS__)
bool8 test_sanity_mmpp_optional() {
    char* NONE = "NONE";
    char* _MMPP_PLACEHOLDER = "PLACEHOLDER";
    TESTCASE("=%d\n", MMPP_DEFER(MMPP_IS_PRESENT)(SOME(1)));
    TESTCASE("=%d\n", MMPP_DEFER(MMPP_IS_PRESENT)(NONE));
    TESTCASE("=%d\n", MMPP_DEFER(IF)(MMPP_IS_PRESENT(SOME(1)))(1, 2));
    TESTCASE("=%d\n", MMPP_DEFER(MMPP_IS_PRESENT)(NONE));

    TESTCASE("=%s\n", MMPP_DEFER(MMPP_AS_OPTIONAL)(SOME(1)));
    TESTCASE("=%s\n", MMPP_DEFER(MMPP_AS_OPTIONAL)(2));

    TESTCASE("=%d\n", MMPP_DEFER(MMPP_OPTIONAL_UNWRAP)(SOME(1)));
    TESTCASE("=%d\n", MMPP_DEFER(MMPP_OPTIONAL_UNWRAP_OR_DEFAULT)(NONE, 3));
    TESTCASE("=%d\n", MMPP_DEFER(MMPP_OPTIONAL_UNWRAP_OR_DEFAULT)(SOME(1), 3));
    return 1;
}

// Logicals
#define _AND_11 SOME(1)
#define AND(x, y) MMPP_OPTIONAL_UNWRAP_OR_DEFAULT(CAT3(_AND_, x, y), 0)

#define _OR_00 SOME(0)
#define OR(x, y) MMPP_OPTIONAL_UNWRAP_OR_DEFAULT(CAT3(_OR_, x, y), 1)

#define _XOR_01 SOME(1)
#define _XOR_10 SOME(1)
#define XOR(x, y) MMPP_OPTIONAL_UNWRAP_OR_DEFAULT(CAT3(_XOR_, x, y), 0)

#define _NOT_0 SOME(1)
#define NOT(x) MMPP_OPTIONAL_UNWRAP_OR_DEFAULT(CAT(_NOT_, x), 0)

void test_sanity_mmpp_logical() {
    printf("cat(a, (x, y))=%s\n", MMPP_DEFERRED_STRINGIFY(CAT(a, (x, y))));

    printf("%d && %d == %d\n", 0, 0, AND(0, 0));
    printf("%d && %d == %d\n", 0, 1, AND(0, 1));
    printf("%d && %d == %d\n", 1, 0, AND(1, 0));
    printf("%d && %d == %d\n", 1, 1, AND(1, 1));
    printf("%d && %d == %d\n", -1, 1, AND(-1, 1));

    printf("%d || %d == %d\n", 0, 0, OR(0, 0));
    printf("%d || %d == %d\n", 0, 1, OR(0, 1));
    printf("%d || %d == %d\n", 1, 0, OR(1, 0));
    printf("%d || %d == %d\n", 1, 1, OR(1, 1));
    printf("%d || %d == %d\n", -1, 1, OR(-1, 1));

    printf("%d xor %d == %d\n", 0, 0, XOR(0, 0));
    printf("%d xor %d == %d\n", 0, 1, XOR(0, 1));
    printf("%d xor %d == %d\n", 1, 0, XOR(1, 0));
    printf("%d xor %d == %d\n", 1, 1, XOR(1, 1));
    printf("%d xor %d == %d\n", -1, 1, XOR(-1, 1));

    printf("!%d == %d\n", 0, NOT(0));
    printf("!%d == %d\n", 1, NOT(1));
    printf("!%d == %d\n", 10, NOT(10));
    printf("!!%d == %d\n", 10, NOT(NOT(10)));
}

// Lists
#define MMPP_HEAD(arg, ...) arg
#define MMPP_TAIL(arg, ...) __VA_ARGS__
#define MMPP_LAST_ITEM SOME(1)
#define MMPP_IS_LIST_EMPTY(...) MMPP_OPTIONAL_UNWRAP_OR_DEFAULT(MMPP_DEFER(MMPP_HEAD)(__VA_ARGS__ MMPP_LAST_ITEM), 0)

#define MMPP_ENCLOSE(...) (__VA_ARGS__)
#define _MMPP_UNPACK(...) __VA_ARGS__
#define MMPP_UNPACK(...) _MMPP_UNPACK __VA_ARGS__

#define IF_ENCLOSED(...) CAT(IF_, IS_ENCLOSED(__VA_ARGS__))

void test_sanity_mmpp_list() {
    printf("%s is empty? %d\n", "(a, b)", MMPP_IS_LIST_EMPTY(a, b));
    printf("%s is empty? %d\n", "()", MMPP_IS_LIST_EMPTY());
    printf("%s is empty? %d\n", "(,,)", MMPP_IS_LIST_EMPTY(,,));
}

//////////////////////////////////////////////////
#ifdef mm_preprocessor

#include "mm_testutils.cpp"

int main(int ArgCount, char **ArgList) {
    // test_sanity_mmpp_optional();
    // test_sanity_mmpp_logical();
    test_sanity_mmpp_list();
    return 0;
}

#endif /* mm_preprocessor */

#endif /* _MM_PREPROCESSOR_C */
#ifndef _MM_ASSERT_H
#define _MM_ASSERT_H

#include <stdio.h>

#define Panic() { *(int *)0 = 0; }

#if MM_ASSERTS
// TODO(MM): add assert messages
#define AssertTrue(Expression) if (!(Expression)) { Panic() }
#define Assert(Expression, Message) if (!(Expression)) {printf(Message); Panic()}
#else
#define AssertTrue(Expression)
#define Assert(Expression, Message)
#endif

#endif /* _MM_ASSERT_H */
#ifndef _MM_FILE_C
#define _MM_FILE_C

#include "mm_types.h"

#ifdef _WIN32
    #include "mm_win.cpp"
#endif

internal inline string mmfile_ReadEntireFile(const char *FileName)
{
    #ifdef _WIN32
        return mmwin_ReadEntireFile(FileName);
    #else
        #error "Not implemented for non-Windows"
    #endif
}


#ifdef mm_file
#include <stdio.h>

int main()
{
    string FileContents = mmfile_ReadEntireFile("mm_file.cpp");
    Assert(FileContents.Length > 100, "File size smaller than expected\n");
    printf(FileContents.Data);
    mmwin_FreeMemory(FileContents.Memory);

    // Test reading from a non-existent file results in null string
    FileContents = mmwin_ReadEntireFile("does_not_exist");
    Assert(FileContents.Length == 0, "Got a non-null result from non-existent file\n");
    return 0;
}
#endif

#endif /* _MM_FILE_C */
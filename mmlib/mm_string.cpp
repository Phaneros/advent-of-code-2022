#ifndef _MM_STRING_C
#define _MM_STRING_C

#include "mm_types.h"
#include "mm_memory.cpp"
#include "mm_list.cpp"

#include <windows.h>

internal s32 mmstring_print(string String)
{
    return printf("%.*s", String.Length, String.Data);
}

internal s32 mmstring_println(string String)
{
    return printf("%.*s\n", String.Length, String.Data);
}

internal s32 mmstring_print(const char *Prefix, string String, const char *Postfix)
{
    return printf("%s%.*s%s", Prefix, String.Length, String.Data, Postfix);
}

internal allocates string mmstring_Allocate(app_memory *AppMemory, s32 Length)
{
    char *DataPointer = (char *)mmem_AllocateObject(AppMemory, Length * sizeof(char));
    return {0, DataPointer};
}

/**
 * Splits a string into an array of non-overlapping string views, separated by Separator
 * 
 * Note(mm): This can be made more efficient with a memory arena, if the returned array is simply allowed to grow freely
 */
internal allocates dynamic_array_string *mmstring_Split(app_memory *AppMemory, string String, char Separator)
{
    u32 NumberOfSegments = 1;
    for (s32 Index = 0; Index < String.Length; ++Index)
    {
        NumberOfSegments += String.Data[Index] == Separator;
    }
    dynamic_array_string *Result = (dynamic_array_string *)mmlist_AllocateList(AppMemory, NumberOfSegments, sizeof(string));
    Result->Length = NumberOfSegments;

    u32 SegmentStartsFound = 1;
    Result->Elements[0].Data = String.Data;
    u32 Index = 0;
    for (; SegmentStartsFound < NumberOfSegments; ++Index)
    {
        if (String.Data[Index] == Separator)
        {
            Result->Elements[SegmentStartsFound-1].Length = (u32)(&String.Data[Index] - Result->Elements[SegmentStartsFound-1].Data);
            SegmentStartsFound++;
            Result->Elements[SegmentStartsFound-1].Data = &String.Data[Index+1];
        }
    }
    Result->Elements[NumberOfSegments-1].Length = String.Length - Index;
    return Result;
}

internal allocates string mmstring_Join(app_memory *AppMemory, dynamic_array_string *Strings)
{
    s32 TotalLength = 0;
    for (s32 Index = 0; Index < Strings->Length; ++Index)
    {
        TotalLength += Strings->Elements[Index].Length;
    }
    string ReturnValue = mmstring_Allocate(AppMemory, TotalLength);
    s32 OutputIndex = 0;
    for (s32 Index = 0; Index < Strings->Length; ++Index)
    {
        memcpy(&ReturnValue.Data[OutputIndex], Strings->Elements[Index].Data, Strings->Elements[Index].Length);
        OutputIndex += Strings->Elements[Index].Length;
    }
    ReturnValue.Length = TotalLength;
    return ReturnValue;
}

internal bool8 mmstring_IsCharDecimal(char Character)
{
    return Character >= '0' && Character <= '9';
}

internal bool8 mmstring_IsCharWhitespace(char Character)
{
    return Character == ' '
        || Character == '\t' // tab
        || Character == '\n' // line feed
        || Character == '\r' // carriage return
        || Character == '\v' // vertical tab
        || Character == '\f' // form feed
    ;
}

internal bool8 mmstring_IsWhitespace(string String)
{
    for (s32 Index = 0; Index < String.Length; ++Index)
    {
        if (!mmstring_IsCharWhitespace(String.Data[Index]))
        {
            return false;
        }
    }
    return true;
}

internal string mmstring_StripWhitespace(string String)
{
    s32 StartIndex = 0;
    while (mmstring_IsCharWhitespace(String.Data[StartIndex]) && StartIndex < String.Length)
    {
        StartIndex++;
    }
    s32 EndIndex = String.Length - 1;
    while (mmstring_IsCharWhitespace(String.Data[EndIndex]) && EndIndex >= StartIndex)
    {
        EndIndex--;
    }
    return {(EndIndex - StartIndex + 1), &String.Data[StartIndex]};
}

internal s32 mmstring_FindFirstOccurrence(string String, char Character)
{
    for (s32 Index = 0; Index < String.Length; ++Index)
    {
        if (String.Data[Index] == Character)
        {
            return Index;
        }
    }
    return -1;
}

internal s32 mmstring_FindLastOccurrence(string String, char Character)
{
    for (s32 Index = String.Length-1; Index >= 0; --Index)
    {
        if (String.Data[Index] == Character)
        {
            return Index;
        }
    }
    return -1;
}

internal string mmstring_Substring(string String, s32 Start, s32 Length)
{
    // # require Length < String.
    Assert(Start >= 0, "Attempted out of bounds access on a string\n");
    Assert(Length > 0, "Attempting to make a negative-length string\n");
    Assert(Start + Length <= String.Length, "Attempted out of bounds access past the end of a string\n");
    return {Length, &String.Data[Start]};
}

internal bool8 mmstring_Equals(string String1, string String2)
{
    if (String1.Length != String2.Length)
    {
        return 0;
    }
    for (s32 Index = 0; Index < String1.Length; ++Index)
    {
        if (String1.Data[Index] != String2.Data[Index])
        {
            return 0;
        }
    }
    return 1;
}

internal optional_s32 mmstring_ParseInt(string String)
{
    s32 Result = 0;
    bool8 IsNegative = false;
    bool8 FirstCharacter = true;
    for (s32 Index = 0; Index < String.Length; ++Index)
    {
        if (mmstring_IsCharWhitespace(String.Data[Index]) || String.Data[Index] == '_')
        {
            continue;
        }
        else if (String.Data[Index] == '-' && FirstCharacter)
        {
            IsNegative = true;
            FirstCharacter = false;
        }
        else if (String.Data[Index] >= '0' && String.Data[Index] <= '9')
        {
            Result = Result * 10 + (String.Data[Index] - '0');
            FirstCharacter = false;
        }
        else
        {
            return {false, 0};
        }
    }
    return {true, IsNegative ? -Result : Result};
}

#ifdef mm_string
#include <stdio.h>
#include "mm_assert.h"

int main()
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    optional_s32 opt_int = mmstring_ParseInt(STATIC_STRING("-1_35"));
    Assert(opt_int.Okay, "Invalid integer format\n");
    printf("%d\n", opt_int.Result);

    string MyString = STATIC_STRING("Hello,, World,!");
    string MyStringArray[1] = {{4, "asdf"}};

    dynamic_array_string *SplitString = mmstring_Split(&AppMemory, MyString, ',');

    printf("%d\n", SplitString->Length);
    for (s32 Index = 0; Index < SplitString->Length; ++Index)
    {
        mmstring_print("\"", SplitString->Elements[Index], "\n");
    }
    printf("equals? %d\n", mmstring_Equals(SplitString->Elements[1], STATIC_STRING("")));

    mmstring_print("stripped=\"", mmstring_StripWhitespace(STATIC_STRING("  12 34\t ")), "\"\n");

    dynamic_array_string *StringsToJoin = (dynamic_array_string *)mmlist_AllocateList(&AppMemory, 3, sizeof(string));
    StringsToJoin->Elements[0] = STATIC_STRING("dir_A");
    StringsToJoin->Elements[1] = STATIC_STRING("/");
    StringsToJoin->Elements[2] = STATIC_STRING("file_B");
    mmstring_print("joined=\"", mmstring_Join(&AppMemory, StringsToJoin), "\"\n");

    return 0;
}
#endif

#endif /* _MM_STRING_C */
#ifndef _MM_MEMORY_H
#define _MM_MEMORY_H

#include "mm_types.h"

#define MM_MAX_ALLOCATIONS (Mebi(1))

typedef s64 mmem_offset_type;

typedef struct all_allocation_info
{
    u64 NumberObjects;
    mmem_offset_type FirstFreeOffset;
} all_allocation_info;

typedef struct allocation_info
{
    mmem_offset_type Offset;
    mmem_offset_type SizeBytes;
} allocation_info;

typedef struct memory_arena
{
    all_allocation_info Info;
    allocation_info ObjectInfo[MM_MAX_ALLOCATIONS];
    u8 Memory[];
} memory_arena;

typedef union memory_pointer
{
    void *Memory;
    u8 *Bytes;
    memory_arena *Arena;
} memory_pointer;

typedef struct app_memory
{
    bool32 IsInitialized;
    s64 PermanentStorageSizeBytes;
    // Note(mm): This should be set to all-zero on startup
    memory_pointer PermanentStorage;

    s64 TransientStorageSizeBytes;
    // Note(mm): This should be set to all-zero on startup
    memory_pointer TransientStorage;
} app_memory;

#endif /* _MM_MEMORY_H */
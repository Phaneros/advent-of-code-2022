
#ifndef _MM_UTIL_C
#define _MM_UTIL_C

#include "mm_assert.h"
#include "mm_types.h"

inline u32
mmutil_SafeTruncateUInt64(u64 Value)
{
    Assert(Value <= 0xFFFFFFFF, "Loss of information converting u64 to u32\n");
    return (u32)Value;
}

#endif /* _MM_UTIL_C */
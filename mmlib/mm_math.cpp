
#ifndef _MM_MATH_C
#define _MM_MATH_C

#include "mm_types.h"

internal s32 mmath_Power(s32 Base, u32 Exponent)
{
    s32 Result = 1;
    for (u32 Iteration = 0; Iteration < Exponent; ++Iteration)
    {
        Result *= Base;
    }
    return Result;
}

#endif /* _MM_FILE_C */


#ifndef _MMWIN_MEMORY_C
#define _MMWIN_MEMORY_C

#include <windows.h>
#include "mm_memory.h"
#include "mm_types.h"
#include "mm_assert.h"

#define MM_DEFAULT_PERMANENT_STORAGE_SIZE (Kibi(64))
#define MM_DEFAULT_TRANSIENT_STORAGE_SIZE (Gibi(1))

internal app_memory mmem_DefaultInitialize(s64 PermanentStorageSizeBytes, s64 TransientStorageSizeBytes)
{
    app_memory AppMemory = {0};
    AppMemory.PermanentStorageSizeBytes = PermanentStorageSizeBytes;
    AppMemory.TransientStorageSizeBytes = TransientStorageSizeBytes;

    u64 TotalSizeBytes = AppMemory.PermanentStorageSizeBytes + AppMemory.TransientStorageSizeBytes;
    #ifdef MM_DEBUG
        LPVOID BaseAddress = (void *)Tebi(1);
    #else
        LPVOID BaseAddress = 0;
    #endif
    AppMemory.PermanentStorage.Memory = VirtualAlloc(
        BaseAddress, (size_t)TotalSizeBytes,
        MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE
    );
    Assert(AppMemory.PermanentStorage.Memory != 0, "Failed to allocate memory\n");
    AppMemory.TransientStorage.Bytes = (AppMemory.PermanentStorage.Bytes + AppMemory.PermanentStorageSizeBytes);
    AppMemory.IsInitialized = true;
    return AppMemory;
}

internal void *mmem_AllocateObject(app_memory *AppMemory, mmem_offset_type ObjectSize)
{
    Assert(AppMemory->IsInitialized, "Memory arena is not initialized\n");
    Assert(AppMemory->TransientStorage.Arena->Info.NumberObjects < MM_MAX_ALLOCATIONS, "Trying to allocate too many objects\n");
    Assert(AppMemory->TransientStorage.Arena->Info.FirstFreeOffset + ObjectSize < AppMemory->TransientStorageSizeBytes, "Not enough space for allocation\n");

    mmem_offset_type FirstFreeOffset = AppMemory->TransientStorage.Arena->Info.FirstFreeOffset;
    AppMemory->TransientStorage.Arena->ObjectInfo[AppMemory->TransientStorage.Arena->Info.NumberObjects].Offset = FirstFreeOffset;
    AppMemory->TransientStorage.Arena->ObjectInfo[AppMemory->TransientStorage.Arena->Info.NumberObjects].SizeBytes = ObjectSize;
    AppMemory->TransientStorage.Arena->Info.NumberObjects++;
    AppMemory->TransientStorage.Arena->Info.FirstFreeOffset += ObjectSize;
    return &AppMemory->TransientStorage.Arena->Memory[FirstFreeOffset];
}

#ifdef MM_DEBUG
#include <stdio.h>
internal void mmem_DEBUG_PrintTransientMemoryStatus(app_memory *AppMemory)
{
    printf("NumObjects: %lld\n", AppMemory->TransientStorage.Arena->Info.NumberObjects);
    printf("Stored: %lld/%lld (bytes)\n", AppMemory->TransientStorage.Arena->Info.FirstFreeOffset, AppMemory->TransientStorageSizeBytes - sizeof(memory_arena));
}
#endif

#ifdef mmwin_memory

#include "mm_list.cpp"
int main()
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);
    dynamic_array_u32 *MyList = (dynamic_array_u32 *)mmem_AllocateObject(&AppMemory, mmlist_SizeOf(10, sizeof(u32)));
    MyList->Length = 10;
    MyList->ElementSize = sizeof(u32);
    MyList->Elements[0] = 9;
    mmem_DEBUG_PrintTransientMemoryStatus(&AppMemory);

    printf("%d\n", *(s32 *)AppMemory.TransientStorage.Arena->Memory);
    printf("%d\n", *(u32 *)(AppMemory.TransientStorage.Arena->Memory + 4));
    printf("%d\n", *(u32 *)(AppMemory.TransientStorage.Arena->Memory + 8));
    printf("%d\n", *(u32 *)(AppMemory.TransientStorage.Arena->Memory + 12));
    printf("%d\n", *(u32 *)(AppMemory.TransientStorage.Arena->Memory + 16));
}
#endif

#endif /* _MMWIN_MEMORY_C */
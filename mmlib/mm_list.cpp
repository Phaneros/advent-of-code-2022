#ifndef _MM_LIST_C
#define _MM_LIST_C

#include <windows.h>
#include "mm_assert.h"
#include "mm_types.h"
#include "mm_math.cpp"
#include "mm_memory.cpp"

internal size_t mmlist_SizeOf(dynamic_array_header *List)
{
    return sizeof(dynamic_array_header) + List->Length * List->ElementSize;
}

internal size_t mmlist_SizeOf(s32 Length, size_t ElementSize)
{
    return sizeof(dynamic_array_header) + Length * ElementSize;
}

internal allocates void *mmlist_AllocateList(app_memory *AppMemory, s32 Length, size_t ElementSize)
{
    Assert(Length >= 0, "List must have positive length\n");
    void *Result = mmem_AllocateObject(AppMemory, mmlist_SizeOf(Length, ElementSize));
    ((dynamic_array_header *)Result)->Length = Length;
    ((dynamic_array_header *)Result)->MaxLength = Length;
    ((dynamic_array_header *)Result)->ElementSize = ElementSize;
    return Result;
}

internal void mmlist_DeallocateList(dynamic_array_header *List)
{
    // VirtualFree(List, mmlist_SizeOf(List), MEM_RELEASE);
}

#define _DEFINE_LESS_THAN(type) \
internal bool8 LessThan_##type(void *a, void *b)\
{\
    return *(type *)a < *(type *)b;\
}
_DEFINE_LESS_THAN(u8)
_DEFINE_LESS_THAN(u16)
_DEFINE_LESS_THAN(u32)
_DEFINE_LESS_THAN(u64)
_DEFINE_LESS_THAN(s8)
_DEFINE_LESS_THAN(s16)
_DEFINE_LESS_THAN(s32)
_DEFINE_LESS_THAN(s64)

internal void *mmlist_MaximumWithComparison(dynamic_array_header *List, FnPointer(LessThanOperation, (void *a, void *b), bool8), void *Default)
{
    if (List->Length == 0)
    {
        return Default;
    }
    void *CurrentMaximum = &List->_Elements[0];
    void *IndexPointer = &List->_Elements[0];
    for (s32 Index = 1; Index < List->Length; ++Index)
    {
        IndexPointer = &List->_Elements[0] + Index * List->ElementSize;
        if (LessThanOperation(CurrentMaximum, IndexPointer))
        {
            CurrentMaximum = IndexPointer;
        }
    }
    return CurrentMaximum;
}

union
{
    u8 _u8;
    u16 _u16;
    u32 _u32;
    u64 _u64;
    s8 _s8;
    s16 _s16;
    s32 _s32;
    s64 _s64;
} mm_Zero;
#define mmlist_Maximum(List, type, Default) (*(type *)mmlist_MaximumWithComparison(&(List)->Header, LessThan_##type, &Default))

#define MM_RADIX_SORT_RADIX (10)
internal void _mmlist_GetRadixSortBucketIndices(dynamic_array_u32 *List, u32 BucketIndices[MM_RADIX_SORT_RADIX], u32 Divisor)
{
    for (s32 Index = 0; Index < List->Length; ++Index)
    {
        BucketIndices[(List->Elements[Index] / Divisor) % MM_RADIX_SORT_RADIX]++;
    }
    u32 CumulativeIndex = 0;
    for (s32 Index = 0; Index < MM_RADIX_SORT_RADIX; ++Index)
    {
        u32 Temp = CumulativeIndex;
        CumulativeIndex += BucketIndices[Index];
        BucketIndices[Index] = Temp;
    }
}

internal allocates void mmlist_RadixSort(app_memory *AppMemory, dynamic_array_u32 *List, u32 MaxDigits)
{
    dynamic_array_u32 *TemporaryList = (dynamic_array_u32 *)mmlist_AllocateList(AppMemory, List->Length, sizeof(s32));
    for (u32 Iteration = 0; Iteration < MaxDigits; ++Iteration)
    {
        u32 BucketIndices[MM_RADIX_SORT_RADIX] = {0};
        u32 Divisor = mmath_Power(MM_RADIX_SORT_RADIX, Iteration);
        _mmlist_GetRadixSortBucketIndices(List, BucketIndices, Divisor);
        for (s32 Index = 0; Index < List->Length; ++Index)
        {
            u32 Bucket = (List->Elements[Index] / Divisor) % MM_RADIX_SORT_RADIX;
            TemporaryList->Elements[BucketIndices[Bucket]++] = List->Elements[Index];
        }
        dynamic_array_u32 *Temp = List;
        List = TemporaryList;
        TemporaryList = Temp;
    }
    if (MaxDigits % 2)
    {
        dynamic_array_u32 *Temp = List;
        List = TemporaryList;
        TemporaryList = Temp;
        memcpy(List, TemporaryList, mmlist_SizeOf(&List->Header));
    }
    mmlist_DeallocateList(&TemporaryList->Header);
}

#ifdef mm_list
#include <stdio.h>
#include "mm_assert.h"

void print_list(dynamic_array_u32 *List)
{
    for (s32 Index = 0; Index < List->Length; ++Index)
    {
        printf("%d, ", List->Elements[Index]);
    }
    printf("\n");
}

int main()
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);
    // u32 Zero = 0;
    dynamic_array_u32 *MyList = (dynamic_array_u32 *)mmlist_AllocateList(&AppMemory, 100, sizeof(u32));
    dynamic_array_u8 *MyU8List = (dynamic_array_u8 *)mmlist_AllocateList(&AppMemory, 100, sizeof(u8));
    dynamic_array_u8 EmptyList = {0, sizeof(u8)};

    Assert(MyList->Length == 100, "Array size doesn't match what was requested\n");
    for (s32 Index = 0; Index < MyList->Length; ++Index)
    {
        MyList->Elements[Index] = (Index * 7) % 109;
    }
    print_list(MyList);
    printf("Max: %d\n", mmlist_Maximum(MyList, u32, mm_Zero));
    mmlist_RadixSort(&AppMemory, MyList, 3);
    print_list(MyList);

    MyU8List->Elements[14] = 201;
    MyU8List->Elements[15] = 200;
    MyU8List->Elements[0] = 100;
    u8 Default = 0;
    printf("%d\n", mmlist_Maximum(MyU8List, u8, mm_Zero));

    printf("Empty max: %d\n", mmlist_Maximum(&EmptyList, u8, mm_Zero));

    return 0;
}
#endif

#endif /* _MM_LIST_C */
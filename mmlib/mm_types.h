#ifndef _MM_TYPES_H
#define _MM_TYPES_H

#include <stdint.h>

#define internal static

// annotations
#define allocates

#define _in
#define _inout
#define _out
#define _move
// Note(mm): 'forward' is also a thing, but isn't applicable outside C++ without significant metaprogramming

// signed
typedef int8_t  s8;   // %hhi or %c
typedef int16_t s16;  // %d
typedef int32_t s32;  // %ld
typedef int64_t s64;  // %lld
// unsigned
typedef uint8_t  u8;  // %hhu or %c
typedef uint16_t u16; // %hu
typedef uint32_t u32; // %u
typedef uint64_t u64; // %llu

typedef float float32;
typedef double float64;
typedef s8 bool8;
typedef s32 bool32;

#define BITS_PER_BYTE (8)
#define Kibi(Value) ((Value) * 1024)
#define Mebi(Value) (Kibi(Value) * 1024)
#define Gibi(Value) (Mebi(Value) * 1024LL)
#define Tebi(Value) (Gibi(Value) * 1024LL)

#define FnPointer(Name, Arguments, ReturnType) ReturnType (*Name)Arguments

typedef struct string
{
    s32 Length;
    union
    {
        void *Memory;
        char *Data;
    };
} string;

#define STATIC_STRING(str) {sizeof(str)-1, str}

typedef struct dynamic_array_header
{
    s32 Length;
    s32 MaxLength;
    size_t ElementSize;
    u8 _Elements[0];
} dynamic_array_header;

#define _DEFINE_ARRAY_OF_TYPE(type) \
typedef struct dynamic_array_##type \
{\
    union {\
        struct {\
            s32 Length;\
            s32 MaxLength;\
            size_t ElementSize;\
            type Elements[0];\
        };\
        struct dynamic_array_header Header;\
    };\
} dynamic_array_##type;

_DEFINE_ARRAY_OF_TYPE(s8)
_DEFINE_ARRAY_OF_TYPE(s16)
_DEFINE_ARRAY_OF_TYPE(s32)
_DEFINE_ARRAY_OF_TYPE(s64)
_DEFINE_ARRAY_OF_TYPE(u8)
_DEFINE_ARRAY_OF_TYPE(u16)
_DEFINE_ARRAY_OF_TYPE(u32)
_DEFINE_ARRAY_OF_TYPE(u64)
_DEFINE_ARRAY_OF_TYPE(string)

#define _DEFINE_OPTIONAL(type) typedef struct optional_##type {bool32 Okay; type Result;} optional_##type;
_DEFINE_OPTIONAL(s32)

#endif // _MM_TYPES_H
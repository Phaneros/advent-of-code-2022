# Requirements
A transpiler from modified C to vanilla C

## Syntactic sugar
* Jai-style variable, function, and array declaration
* Type-inference
* Better numeric literals
* Get rid of `->`?
  * Maybe use `>>.` instead
* Tuples
* Multiple return values
* Special handling of strings
* Defer?

### Free symbols
* `:` is used by digraphs, ternary statements, and C++ inheritance / initialization lists, repurpose?
* `>>`, `<<` are bitshifts / C++, could steal
* `$` for type templates?
* `@` is unused
* backtick is unused (`` ` ``), but a pain to put in markdown
* Rust uses `#[]` declarations above structs / functions, could be useful
* `is` / `as` -- @Herb Sutter
* `in` ?

### Keywords
* #assume -- Give a hint to the static analyzer
* #require -- For detailed type relationships / static checks
* #deprecated ?

## Simplify
* Block trigraphs / digraphs
* Remove `unsigned`, `signed`, `long` modifiers
* Remove `nullptr`?
* Remove base `int` types -- replace with `u8`, `u16`, `u32`, `u64`, `s8`, `s16`, `s32`, `s64`

## Static Analysis
* Better type-checking
* Bounds-check at compile-time
* Static asserts
  * (todo: research C++ concepts)
* Checks on parameters (in, out, inout, move)

### Typing
* Provide stronger typing than C
* Add dynamic types on request
  * e.g. optional / variant

### Constexpr
* Make this purely implicit?
* Literals, and transformations of purely literal / values are constexpr
* anything from command-line args, IO, volatile, is non-constexpr

## Generation
* Use compile-time types
* Generate runtime types
  * See C++ `std::type_info` / `typeid()`
* Be able to generate repeated / templated functions and structures
  * @Note maybe decide based on complexity between generating a new function vs. passing a runtime type constexpr
* Pattern matching?

## Example
```C
import stdio::printf;

trait numeric;   // built-in; includes uints, sints, and floats + operators
trait unsigned;  // built-in; includes uints
trait integral;  // built-in
trait pointer;   // built-in
trait pointer<$T>;  // built-in

trait even
{
    #require _ is integral;
    #require _ % 2 == 0;
};

trait has_alloc
{
    #require _ ::allocate : () -> pointer;
};

struct my_struct
{
    s32 MyValue;
}
#assume

SumList : (in List : $T[]) -> $T  // $T has scope in this function
    #require $T is numeric;    // #require can go anywhere $T is in scope
    #require $T.length < 100;  // verified at static-analysis step
                                // if unverifiable (e.g. variable-size arrays), emits a runtime check
{
    $T Sum;
    for (Item in List)
    {
        Sum += Item;
    }
    return Sum;
}

main : (ArgCount: s32, ArgList: string[]) -> s32
{
    MyInt : s32 = 4;               // Implicitly constexpr
    MyU16Array : u16[MyInt] = {};  // Implicitly constant-size
    MyU16Array[1] = 1;
    MyU16Array[2] = ArgCount;      // Warns about type coercion unless suppressed with `assume`
    u16 Sum = SumList(MyU16Array);
    return 0;
}
```

Converts to:
```C
typedef struct my_struct
{
    s32 MyValue;
} my_struct;

u16 SumList_u16(u16 List[4])  // deduces most precise output
{
    u16 Sum = 0;
    for (s32 _List_Index_0 = 0; _List_Index_0 < 4; ++_List_Index_0)
    {
        u16 Item = List[_List_Index_0];
        Sum += Item;
    }
    return Sum;
}

int main(int ArgCount, char **ArgList) {
    s32 MyInt = 4;
    u16 MyU16Array[4] = {0};
    MyU16Array[1] = 1;
    MyU16Array[2] = ArgCount;
    u16 Sum = SumList_u16(MyU16Array);
    return 0;
}
```

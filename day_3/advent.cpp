
#include <stdio.h>

#include "mm_file.cpp"
#include "mm_memory.cpp"
#include "mm_string.cpp"
#include "mm_list.cpp"

internal char mmstring_FirstCharInCommon(string String1, string String2, char Default)
{
    for (s32 Index2 = 0; Index2 < String2.Length; ++Index2)
    {
        for (s32 Index1 = 0; Index1 < String1.Length; ++Index1)
        {
            if (String1.Data[Index1] == String2.Data[Index2])
            {
                return String1.Data[Index1];
            }
        }
    }
    return Default;
}

s32 priority(char Item)
{
    if (Item >= 'a' && Item <= 'z')
    {
        return 1 + Item - 'a';
    }
    else
    {
        return 27 + Item - 'A';
    }
}

#define ITEM_VARIETIES (26 * 2)

internal void ResetHistogram(_out bool8 Histogram[ITEM_VARIETIES])
{
    for (s32 Index = 0; Index < ITEM_VARIETIES; ++Index)
    {
        Histogram[Index] = 0;
    }
}

internal bool8 *HistogramIt(_out bool8 Histogram[ITEM_VARIETIES], string String)
{
    for (s32 Index = 0; Index < String.Length; ++Index)
    {
        Histogram[priority(String.Data[Index])-1] = 1;
    }
    return Histogram;
}

internal s32 FirstNonzeroIndex(_in bool8 Histogram[ITEM_VARIETIES])
{
    for (s32 Index = 0; Index < ITEM_VARIETIES; ++Index)
    {
        if (Histogram[Index])
        {
            return Index;
        }
    }
    AssertTrue(0);
    return -1;
}

internal void AndHistograms(_inout bool8 Histogram1[ITEM_VARIETIES], _move bool8 Histogram2[ITEM_VARIETIES])
{
    for (s32 Index = 0; Index < ITEM_VARIETIES; ++Index)
    {
        Histogram1[Index] = Histogram1[Index] * Histogram2[Index];
    }
}

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);

    string Input = mmfile_ReadEntireFile("input.txt");
    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    s32 TotalPriority = 0;
    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (Line.Length == 0)
        {
            continue;
        }
        string FirstHalf = {Line.Length / 2, Line.Data};
        string SecondHalf = {Line.Length / 2, Line.Data + Line.Length / 2};
        char ItemInCommon = mmstring_FirstCharInCommon(FirstHalf, SecondHalf, 0);
        AssertTrue(ItemInCommon != 0);
        TotalPriority += priority(ItemInCommon);
        printf("%d\n", priority(ItemInCommon));
    }
    printf("%d\n", TotalPriority);
    // @ans: 7742
    // 21:23

    // Part 2
    s32 TotalBadgePriority = 0;
    bool8 PrimaryHistogram[ITEM_VARIETIES];
    bool8 SecondaryHistogram[ITEM_VARIETIES];
    for (s32 LineNumber = 0; LineNumber < Lines->Length; ++LineNumber)
    {
        string Line = Lines->Elements[LineNumber];
        if (Line.Length == 0)
        {
            continue;
        }
        if (LineNumber % 3 == 0)
        {
            if (LineNumber > 0)
            {
                s32 Priority = FirstNonzeroIndex(PrimaryHistogram) + 1;
                printf("%d ", Priority);
                TotalBadgePriority += Priority;
            }
            ResetHistogram(PrimaryHistogram);
            HistogramIt(PrimaryHistogram, Line);
        }
        else
        {
            ResetHistogram(SecondaryHistogram);
            HistogramIt(SecondaryHistogram, Line);
            AndHistograms(PrimaryHistogram, SecondaryHistogram);
        }
    }
    {
        s32 Priority = FirstNonzeroIndex(PrimaryHistogram) + 1;
        printf("%d ", Priority);
        TotalBadgePriority += Priority;
    }
    printf("%d\n", TotalBadgePriority);
    // @ans: 2276
    // 22:05
    // would be nice to clean up, the off-by-ones were killer

    return 0;
}

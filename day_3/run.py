from typing import *

from common import load_input
import numpy as np


def priority(item: str) -> int:
    if item.islower():
        return ord(item) - ord('a') + 1
    else:
        return ord(item) - ord('A') + 27


if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    total = 0
    for line in lines:
        num_items = len(line)
        assert num_items % 2 == 0
        half_size = num_items // 2
        first_half = set(line[:half_size])
        common = first_half.intersection(line[half_size:])
        assert len(common) == 1
        common_item = [x for x in common][0]
        total += priority(common_item)
        # print(f'{common_item}, {priority(common_item)}')
    print(total)
    # 6 mins!
    # @ans: 7742

    # part 2
    group = -1
    common = set()
    total_priority = 0
    for line_number, line in enumerate(lines):
        if line_number % 3 == 0:
            group += 1
            if len(common):
                assert len(common) == 1
                total_priority += priority([x for x in common][0])
            common = set(line)
        else:
            common = common.intersection(line)
    assert len(common) == 1
    total_priority += priority([x for x in common][0])
    print(total_priority)
    
    # @ans: 2276


from typing import *

from common import load_input
import numpy as np
import re
from dataclasses import dataclass

@dataclass
class Valve:
    valve_id: str
    flow_rate: int
    leads_to: List[str]

if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')

    total_time = 30  # in min
    # flow rate in pressure / min
    line_pattern = re.compile(r'Valve (\w\w) has flow rate=(\d+); tunnels? leads? to valves? (.*)')

    valves: Dict[str, Valve] = {}
    
    
    for line in lines:
        match = re.match(line_pattern, line)
        assert match is not None
        valve_id, flow_rate_str, leads_to_str = match.groups()
        valves[valve_id] = Valve(valve_id=valve_id, flow_rate=int(flow_rate_str), leads_to=leads_to_str.split(', '))

    pressure_released = 0
    open_valves: Set[str] = set()
    current_pos: str = 'AA'
    MAX_STATES = 10000

    @dataclass(frozen=True)
    class State:
        pos: str
        opened: Tuple[str, ...]
        value: int
        cost: int
    states = [set([State('AA', (), 0, 0)])]
    for minute in range(0):
    # for minute in range(total_time):
        states.append(set())
        for valve in valves.values():
            not_move_possibilities = [x for x in states[-2] if x.pos == valve.valve_id]
            for possibility in not_move_possibilities:
                if valve.valve_id not in possibility.opened and valve.flow_rate > 0:
                    states[-1].add(State(
                        valve.valve_id,
                        tuple(sorted(possibility.opened + (valve.valve_id,))),
                        value=possibility.value + sum([valves[vid].flow_rate for vid in possibility.opened]),
                        cost=possibility.cost + 1
                    ))
        for state in states[-2]:
            for next_pos in valves[state.pos].leads_to:
                states[-1].add(State(
                    next_pos,
                    state.opened,
                    state.value + sum([valves[vid].flow_rate for vid in state.opened]),
                    cost=state.cost + 1))
        if len(states[-1]) > MAX_STATES:
            ordered_states = sorted(states[-1], key=lambda x: x.value)
            states[-1] = set(ordered_states[-MAX_STATES:])
        print(f'Completed iteration {minute}')
    print(max(states[-1], key=lambda x: x.value))
    # @ans: 1986

    # Part 2
    @dataclass(frozen=True)
    class StateTwo:
        pos: Tuple[str, ...]
        opened: Tuple[str, ...]
        value: int

    total_time = 26
    states = [set([StateTwo(('AA', 'AA'), (), 0)])]
    for minute in range(total_time):
        states.append(set())
        for state in states[-2]:
            for next_pos in valves[state.pos[0]].leads_to:
                # both move
                for next_elephant_pos in valves[state.pos[1]].leads_to:
                    states[-1].add(StateTwo(
                        tuple(sorted((next_pos, next_elephant_pos))),
                        state.opened,
                        state.value + sum([valves[vid].flow_rate for vid in state.opened])
                    ))
                    # only elephant moves, I open
                    if state.pos[0] not in state.opened and valves[state.pos[0]].flow_rate > 0:
                        states[-1].add(StateTwo(
                            tuple(sorted((state.pos[0], next_elephant_pos))),
                            tuple(sorted(state.opened + (state.pos[0],))),
                            state.value + sum([valves[vid].flow_rate for vid in state.opened])
                        ))
                # only I move, elephant opens
                if state.pos[1] not in state.opened and valves[state.pos[1]].flow_rate > 0:
                    states[-1].add(StateTwo(
                        tuple(sorted((next_pos, state.pos[1]))),
                        tuple(sorted(state.opened + (state.pos[1],))),
                        state.value + sum([valves[vid].flow_rate for vid in state.opened])
                    ))
            # both open, neither move
            if (
                state.pos[0] not in state.opened
                and state.pos[1] not in state.opened
                and valves[state.pos[0]].flow_rate > 0
                and valves[state.pos[1]].flow_rate > 0
                and state.pos[0] != state.pos[1]
            ):
                states[-1].add(StateTwo(
                    state.pos,
                    tuple(sorted(state.opened + state.pos)),
                    state.value + sum([valves[vid].flow_rate for vid in state.opened])
                ))

        if len(states[-1]) > MAX_STATES:
            ordered_states = sorted(states[-1], key=lambda x: x.value)
            states[-1] = set(ordered_states[-MAX_STATES:])
        print(f'Completed iteration {minute}')
    print(max(states[-1], key=lambda x: x.value))
    # @ans: 2464
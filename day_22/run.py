from typing import *

from common import load_input
import numpy as np
import sys
import re
from dataclasses import dataclass

np.set_printoptions(threshold=sys.maxsize, linewidth=10000)

@dataclass
class Range:
    minimum: int
    maximum: int

def final_password(row: int, column: int, facing: int) -> int:
    return 1000 * (row + 1) + 4 * (column + 1) + facing

if __name__ == '__main__':
    lines = load_input.load_local_input_nostrip(__file__, 'input.txt')
    # lines = load_input.load_local_input_nostrip(__file__, 'test.txt')
    

    board_lines = lines[:-2]
    command_line = lines[-1]

    board_height = len(board_lines)
    board_width = max(len(line) for line in board_lines)

    board = np.zeros((board_width, board_height), dtype=np.int8)

    EMPTY = 0
    PATHABLE = 1
    WALL = 2

    cell_map = {
        ' ': EMPTY,
        '.': PATHABLE,
        '#': WALL,
    }

    row_max = np.array([len(line) - 1 for line in board_lines], dtype=np.int32)
    row_min = np.zeros((board_height,), dtype=np.int32)

    rows: List[int] = []
    for line_num, line in enumerate(board_lines):
        board[:len(line), line_num] = [cell_map[c] for c in line]
        row_min[line_num] = np.min(np.nonzero(board[:, line_num]))

    column_max = np.zeros((board_width,), dtype=np.int32)
    column_min = np.zeros((board_width,), dtype=np.int32)
    for column_num in range(board_width):
        nonzero_indices = np.nonzero(board[column_num, :])
        column_max[column_num] = np.max(nonzero_indices)
        column_min[column_num] = np.min(nonzero_indices)
        

    order_pattern = re.compile(r'(\d+|R|L)')

    RIGHT = 0
    DOWN = 1
    LEFT  = 2
    UP = 3
    NUM_DIRECTIONS = 4
    directions = {
        RIGHT: np.array([+1, 0], dtype=np.int32),
        LEFT:  np.array([-1, 0], dtype=np.int32),
        DOWN:  np.array([0, +1], dtype=np.int32),
        UP:    np.array([0, -1], dtype=np.int32),
    }

    orders: List[str] = re.findall(order_pattern, command_line)
    
    # part 1
    position = np.array([row_min[0], 0], dtype=np.int32)
    facing = RIGHT

    for order in orders:
        if order == 'R':
            facing = (facing + 1) % 4
        elif order == 'L':
            facing = (facing + 4 - 1) % 4
        elif order.isnumeric():
            for step in range(int(order)):
                new_pos = position + directions[facing]
                if np.any(new_pos < 0) or np.any(new_pos >= [board_width, board_height]) or board[new_pos[0], new_pos[1]] == EMPTY:
                    if facing == RIGHT:
                        new_pos[0] = row_min[new_pos[1]]
                    elif facing == LEFT:
                        new_pos[0] = row_max[new_pos[1]]
                    elif facing == DOWN:
                        new_pos[1] = column_min[new_pos[0]]
                    elif facing == UP:
                        new_pos[1] = column_max[new_pos[0]]
                    assert board[new_pos[0], new_pos[1]] != EMPTY
                if board[new_pos[0], new_pos[1]] == WALL:
                    break
                position = new_pos
        else:
            assert False
        # print(f'After order {order}, position= {position}, facing= {facing}')
    
    pwd = final_password(position[1], position[0], facing)
    print(position)
    print(facing)
    print(pwd)
    # @ans: 55244

    # Part 2
    # x, y, facing
    position = np.array([row_min[0], 0, RIGHT], dtype=np.int32)

    # face map
    # the global format is
    # 405
    # .1.
    # .2.
    # .3.

    # (face, direction) -> new_face, new_direction
    face_connections = {
        (0, UP): (3, UP),
        (0, RIGHT): (5, RIGHT),
        (0, LEFT): (4, LEFT),
        (0, DOWN): (1, DOWN),
        (1, UP): (0, UP),
        (1, RIGHT): (5, UP),
        (1, LEFT): (4, UP),
        (1, DOWN): (2, DOWN),
        (2, UP): (1, UP),
        (2, RIGHT): (5, LEFT),
        (2, LEFT): (4, RIGHT),
        (2, DOWN): (3, DOWN),
        (3, UP): (2, UP),
        (3, RIGHT): (5, DOWN),
        (3, LEFT): (4, DOWN),
        (3, DOWN): (0, DOWN),
        (4, UP): (3, RIGHT),
        (4, RIGHT): (0, RIGHT),
        (4, LEFT): (2, RIGHT),
        (4, DOWN): (1, RIGHT),
        (5, UP): (3, LEFT),
        (5, RIGHT): (2, LEFT),
        (5, LEFT): (0, LEFT),
        (5, DOWN): (1, LEFT),
    }

    # test data
    # SIDE_LENGTH = 4
    # face_map = np.array([
    #         [-1, -1, 0, -1],
    #         [ 3,  4, 1, -1],
    #         [-1, -1, 2,  5],
    # ], dtype=np.int32)
    # face_map = face_map.T
    # # which local direction is right in global coordinates?
    # spin_map = {
    #     0: RIGHT,
    #     1: RIGHT,
    #     2: RIGHT,
    #     3: LEFT,
    #     4: UP,
    #     5: LEFT,
    # }


    SIDE_LENGTH = 50
    face_map = np.array([
            [-1,  0,  5],
            [-1,  1, -1],
            [ 4,  2, -1],
            [ 3, -1, -1],
    ], dtype=np.int32)
    face_map = face_map.T
    # which local direction is right in global coordinates?
    spin_map = {
        0: RIGHT,
        1: RIGHT,
        2: RIGHT,
        3: DOWN,
        4: LEFT,
        5: RIGHT,
    }

    # whether crossing a boundary flips the position offset from left / top to right / bottom
    _counting_direction = {
        RIGHT: +1,
        DOWN: -1,
        LEFT: -1,
        UP: +1,
    }
    def new_side_pos(old_side_pos: int, old_dir: int, new_dir: int) -> np.ndarray:
        # return (old_side_pos)
        flipping_factor = _counting_direction[old_dir] * _counting_direction[new_dir]
        if flipping_factor > 0:
            return old_side_pos
        return ((-old_side_pos - 1) % SIDE_LENGTH + SIDE_LENGTH) % SIDE_LENGTH

    def update_position(old_pos: np.ndarray, amount: int) -> np.ndarray:
        position = old_pos.copy()
        for step in range(amount):
            new_pos = position.copy()
            new_pos[:2] = position[:2] + directions[position[2]]
            if (np.any(new_pos < 0)
                or np.any(new_pos >= [board_width, board_height, 4])
                or board[new_pos[0], new_pos[1]] == EMPTY
            ):
                old_direction = position[2]
                squeezed_old_pos = position[:2] // SIDE_LENGTH
                pos_in_face = position[:2] % SIDE_LENGTH
                if old_direction == UP or old_direction == DOWN:
                    pos_along_leaving_axis = pos_in_face[0]
                else:
                    pos_along_leaving_axis = pos_in_face[1]

                face = face_map[squeezed_old_pos[0], squeezed_old_pos[1]]
                assert face >= 0
                global_direction = (old_direction - spin_map[face] + NUM_DIRECTIONS) % NUM_DIRECTIONS

                new_face, new_global_direction = face_connections[(face, global_direction)]
                new_direction = (new_global_direction + spin_map[new_face]) % NUM_DIRECTIONS
                new_face_upper_left = np.argwhere(face_map == new_face) * SIDE_LENGTH
                assert new_face_upper_left.shape == (1, 2)

                pos_along_arriving_axis = new_side_pos(pos_along_leaving_axis, old_direction, new_direction)

                new_coords = new_face_upper_left[0, :]
                if new_direction == RIGHT:
                    new_coords[1] += pos_along_arriving_axis
                elif new_direction == LEFT:
                    new_coords[0] += SIDE_LENGTH - 1
                    new_coords[1] += pos_along_arriving_axis
                elif new_direction == UP:
                    new_coords[1] += SIDE_LENGTH - 1
                    new_coords[0] += pos_along_arriving_axis
                elif new_direction == DOWN:
                    new_coords[0] += pos_along_arriving_axis
                new_pos = np.array([new_coords[0], new_coords[1], new_direction], dtype=np.int32)
            if board[new_pos[0], new_pos[1]] == WALL:
                return position
            position = new_pos
        return position

    orders_completed = 0
    for order in orders:
        if order == 'R':
            position[2] = (position[2] + 1) % NUM_DIRECTIONS
        elif order == 'L':
            position[2] = (position[2] + NUM_DIRECTIONS - 1) % NUM_DIRECTIONS
        elif order.isnumeric():
            position = update_position(position, int(order))
        else:
            assert False
        # print(f'After order {order}, position= {position[:2]}, facing= {position[2]}')

    print('==== PART 2 ====')
    pwd = final_password(position[1], position[0], position[2])
    print(position)
    print(pwd)
    # @ans: 123149
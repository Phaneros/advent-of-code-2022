
#include <stdio.h>
#include "mm_assert.h"
#include "mm_file.cpp"
#include "mm_string.cpp"
#include "mm_memory.cpp"

int main(int ArgCount, char **ArgList)
{
    app_memory AppMemory = mmem_DefaultInitialize(MM_DEFAULT_PERMANENT_STORAGE_SIZE, MM_DEFAULT_TRANSIENT_STORAGE_SIZE);
    string Input = mmfile_ReadEntireFile("input.txt");

    dynamic_array_string *Lines = mmstring_Split(&AppMemory, Input, '\n');

    // Part 1
    u32 Score = 0;
    for (s32 Index = 0; Index < Lines->Length; ++Index)
    {
        string Line = mmstring_StripWhitespace(Lines->Elements[Index]);
        if (Line.Length != 3)
        {
            continue;
        }
        char OpponentChoice = Line.Data[0];
        char MyChoice = Line.Data[2] - 'X' + 'A';

        // for my choice
        Score += 1 + MyChoice - 'A';

        // for the win / loss / tie
        s8 WinState = (MyChoice - OpponentChoice + 1 + 3) % 3;
        Score += 3 * WinState;
    }
    printf("%d\n", Score);
    // @ans: 15572

    // Part 2
    Score = 0;
    for (s32 Index = 0; Index < Lines->Length; ++Index)
    {
        string Line = mmstring_StripWhitespace(Lines->Elements[Index]);
        if (Line.Length != 3)
        {
            continue;
        }
        char OpponentChoice = Line.Data[0];
        char Outcome = Line.Data[2] - 'X'; // 0=loss, 1=tie, 2=win
        char MyChoice = OpponentChoice + Outcome - 1;
        if (MyChoice > 'C')
        {
            MyChoice -= 3;
        }
        if (MyChoice < 'A')
        {
            MyChoice += 3;
        }

        // for my choice
        Score += 1 + MyChoice - 'A';

        // for the win / loss / tie
        s8 WinState = (MyChoice - OpponentChoice + 1 + 3) % 3;
        Score += 3 * WinState;
    }
    printf("%d\n", Score);
    // @ans: 16098
    // Note(mm): Got this working at 22:07, much better than yesterday
    return 0;
}

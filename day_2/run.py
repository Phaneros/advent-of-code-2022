from typing import *

from common import load_input
import numpy as np


if __name__ == '__main__':
    lines = load_input.load_local_input(__file__, 'input.txt')
    # lines = load_input.load_local_input(__file__, 'test.txt')
    
    choice_score = {
        'X': 1, 'Y': 2, 'Z': 3,
    }
    win_score = {
        'AX': 3, 'AY': 6, 'AZ': 0,
        'BX': 0, 'BY': 3, 'BZ': 6,
        'CX': 6, 'CY': 0, 'CZ': 3,
    }
    cumulative_score = 0
    for line in lines:
        opponent, mine = line.split(' ')
        cumulative_score += choice_score[mine] + win_score[opponent + mine]

    print(cumulative_score)
    # @ans: 15572

    # part 2
    # X=lose, Y=draw, Z=win
    match_score = {
        'AX': 0 + 3, 'AY': 3 + 1, 'AZ': 6 + 2,
        'BX': 0 + 1, 'BY': 3 + 2, 'BZ': 6 + 3,
        'CX': 0 + 2, 'CY': 3 + 3, 'CZ': 6 + 1,
    }
    new_cumulative_score = 0
    # for line in ['A Y', 'B X', 'C Z']:
    for line in lines:
        opponent, outcome = line.split(' ')
        new_cumulative_score += match_score[opponent + outcome]
    print(new_cumulative_score)

    # @ans: 16098
    # This took me like 15 minutes as I kept on mixing up my grid -__-
